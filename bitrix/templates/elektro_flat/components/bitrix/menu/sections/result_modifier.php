<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!CModule::IncludeModule("iblock"))
	return;

if(count($arResult) < 1)
	return;

$sectionName = array();
$sectionPicture = array();

foreach($arResult as $key => $arItem) {
	if($arItem["DEPTH_LEVEL"] == 2) {
		$sectionName[] = $arItem["TEXT"];
	}
}

$dbSections = CIBlockSection::GetList(
	array(),
	array("NAME" => $sectionName),
	false,
	array("NAME", "PICTURE")
);
while($arSections = $dbSections->GetNext()) {
	$sectionPicture[$arSections["NAME"]] = $arSections["PICTURE"];
}

foreach($arResult as $key => $arItem) {	
	if($arItem["DEPTH_LEVEL"] == 2) {
		if(!empty($sectionPicture[$arItem["TEXT"]])) {
			$arFileTmp = CFile::ResizeImageGet(
				$sectionPicture[$arItem["TEXT"]],
				array("width" => 50, "height" => 50),
				BX_RESIZE_IMAGE_PROPORTIONAL,
				true
			);
			$arResult[$key]["PICTURE"] = array(
				"SRC" => $arFileTmp["src"],
				"WIDTH" => $arFileTmp["width"],
				"HEIGHT" => $arFileTmp["height"],
			);
		}
	}
}?>