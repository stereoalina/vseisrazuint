<?
$MESS["AUTH_AUTHORIZE"] = "Autoryzuj";
$MESS["AUTH_CAPTCHA_PROMT"] = "Wpisz tekst z obrazu";
$MESS["AUTH_FIRST_ONE"] = "Je?li odwiedzasz witryn? po raz pierwszy, wype?nij formularz rejestracyjny.";
$MESS["AUTH_FORGOT_PASSWORD_2"] = "Nie pami?tasz has?a?";
$MESS["AUTH_LOGIN"] = "Login:";
$MESS["AUTH_NONSECURE_NOTE"] = "Has?o zostanie wys?ane w formie otwartej. Aby w??czy? szyfrowanie has?a, w??cz obs?ug? JavaScript w przegl?darce internetowej.";
$MESS["AUTH_PASSWORD"] = "Has?o:";
$MESS["AUTH_PLEASE_AUTH"] = "Autoryzuj:";
$MESS["AUTH_REGISTER"] = "Zarejestruj";
$MESS["AUTH_REMEMBER_ME"] = "Zapami?taj mnie na tym komputerze";
$MESS["AUTH_SECURE_NOTE"] = "Has?o b?dzie szyfrowane przed wys?aniem. Uniemo?liwi to pojawienie si? has?a w otwartej formie w kana?ach transmisji danych.";
$MESS["AUTH_TITLE"] = "Zaloguj";
?>