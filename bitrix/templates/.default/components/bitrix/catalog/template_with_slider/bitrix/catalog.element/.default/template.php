<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);

global $arSetting; ?>




<? $strMainID = $this->GetEditAreaId($arResult["ID"]);
$arItemIDs = array(
    "ID" => $strMainID,
    "PICT" => $strMainID . "_picture",
    "PRICE" => $strMainID . "_price",
    "BUY" => $strMainID . "_buy",
    "DELAY" => $strMainID . "_delay",
    "STORE" => $strMainID . "_store",
    "PROP_DIV" => $strMainID . "_skudiv",
    "PROP" => $strMainID . "_prop_",
    "SELECT_PROP_DIV" => $strMainID . "_propdiv",
    "SELECT_PROP" => $strMainID . "_select_prop_",
);
$strObName = "ob" . preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
$templateData["JS_OBJ"] = $strObName;

$sticker = "";
$timeBuy = "";
if (array_key_exists("PROPERTIES", $arResult) && is_array($arResult["PROPERTIES"])):
    /***NEW***/
    if (
        array_key_exists("NEWPRODUCT", $arResult["PROPERTIES"]) &&
        !$arResult["PROPERTIES"]["NEWPRODUCT"]["VALUE"] == false):
        $sticker .= "<span class='new'>" . GetMessage("CATALOG_ELEMENT_NEWPRODUCT") . "</span>";
    endif;
    /***HIT***/
    if (
        array_key_exists("SALELEADER", $arResult["PROPERTIES"]) &&
        !$arResult["PROPERTIES"]["SALELEADER"]["VALUE"] == false):
        $sticker .= "<span class='hit'>" . GetMessage("CATALOG_ELEMENT_SALELEADER") . "</span>";
    endif;
    /***SALE***/
    if (
        array_key_exists("SALE", $arResult["PROPERTIES"]) &&
        !$arResult["PROPERTIES"]["SALE"]["VALUE"] == false):
        $sticker .= "<span class='sale'>" . GetMessage("CATALOG_ELEMENT_SALE") . "</span>";
    endif;
    /***DISCOUNT***/
    if (isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])):
        if ($arSetting["OFFERS_VIEW"]["VALUE"] == "LIST"):
            if ($arResult["TOTAL_OFFERS"]["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"] > 0):
                $sticker .= "<span class='discount'>-" .
                    $arResult["TOTAL_OFFERS"]["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"] . "%</span>";
            else:
                if (
                    array_key_exists("DISCOUNT", $arResult["PROPERTIES"]) &&
                    !$arResult["PROPERTIES"]["DISCOUNT"]["VALUE"] == false):
                    $sticker .= "<span class='discount'>%</span>";
                endif;
            endif;
        endif;
    else:
        if ($arResult["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"] > 0):
            $sticker .= "<span class='discount'>-" . $arResult["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"] . "%</span>";
        else:
            if (
                array_key_exists("DISCOUNT", $arResult["PROPERTIES"]) &&
                !$arResult["PROPERTIES"]["DISCOUNT"]["VALUE"] == false):
                $sticker .= "<span class='discount'>%</span>";
            endif;
        endif;
    endif;
    /***TIME_BUY***/
    if (
        array_key_exists("TIME_BUY", $arResult["PROPERTIES"]) &&
        !$arResult["PROPERTIES"]["TIME_BUY"]["VALUE"] == false):
        if (!empty($arResult["CURRENT_DISCOUNT"]["ACTIVE_TO"])):
            if (isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])):
                $timeBuy = "<span class='time_buy_figure'></span><span class='time_buy_text'>" .
                    GetMessage("CATALOG_ELEMENT_TIME_BUY") . "</span>";
            else:
                if ($arResult["CAN_BUY"]):
                    $timeBuy = "<span class='time_buy_figure'></span><span class='time_buy_text'>" .
                        GetMessage("CATALOG_ELEMENT_TIME_BUY") . "</span>";
                endif;
            endif;
        endif;
    endif;
endif; ?>


<div id="<?= $arItemIDs['ID'] ?>" class="catalog-detail-element" itemscope itemtype="http://schema.org/Product">
    <? if (isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])): ?>
        <ul class="offer-title-list">
            <? foreach ($arResult["OFFERS"] as $arOffer): ?>
                <li id="offer_title_<?= $arOffer['ID'] ?>"
                    class="offer_title offer_title_<?= $arOffer['ID'] ?> <?= $arResult['ID'] ?> hidden">
                    <?= $arOffer["NAME"] ?>
                </li>
            <? endforeach ?>
        </ul>
    <? else: ?>
        <h1><?= $APPLICATION->ShowTitle(false); ?></h1>
    <? endif; ?>
    <meta content="<?= $arResult['NAME'] ?>" itemprop="name"/>
    <div class="catalog-detail">
        <div class="column first">
            <div class="catalog-detail-pictures">
                <div class="catalog-detail-picture" id="<?= $arItemIDs['PICT'] ?>">
                    <? /***PICTURE***/
                    if (isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])):
                        if ($arSetting["OFFERS_VIEW"]["VALUE"] != "LIST"):
                            foreach ($arResult["OFFERS"] as $key => $arOffer):?>
                                <div id="detail_picture_<?= $arOffer['ID'] ?>"
                                     class="detail_picture <?= $arResult['ID'] ?> hidden">
                                    <meta content="<?= is_array($arOffer['DETAIL_IMG']) ?
                                        $arOffer['DETAIL_PICTURE']['SRC'] : $arResult['DETAIL_PICTURE']['SRC'] ?>"
                                          itemprop="image"/>
                                    <a rel="lightbox" class="catalog-detail-images"
                                       id="catalog-detail-images-<?= $arOffer['ID'] ?>"
                                       href="<?= $arOffer['DETAIL_IMG']['SRC'] ?>">
                                        <? if (is_array($arOffer["DETAIL_IMG"])): ?>
                                            <img src="<?= $arOffer['DETAIL_IMG']['SRC'] ?>"
                                                 width="<?= $arOffer['DETAIL_IMG']['WIDTH'] ?>"
                                                 height="<?= $arOffer['DETAIL_IMG']['HEIGHT'] ?>"
                                                 alt="<?= $arOffer['NAME'] ?>"/>
                                        <? else: ?>
                                            <img src="<?= $arResult['DETAIL_IMG']['SRC'] ?>"
                                                 width="<?= $arResult['DETAIL_IMG']['WIDTH'] ?>"
                                                 height="<?= $arResult['DETAIL_IMG']['HEIGHT'] ?>"
                                                 alt="<?= $arOffer['NAME'] ?>"/>
                                        <? endif; ?>
                                        <div class="time_buy_sticker">
                                            <?= $timeBuy ?>
                                        </div>
                                        <div class="sticker">
                                            <?= $sticker;
                                            if ($arOffer["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"] > 0):?>
                                                <span class="discount">-<?= $arOffer["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"] ?>
                                                    %</span>
                                            <?
                                            else:
                                                if (
                                                    array_key_exists("DISCOUNT", $arResult["PROPERTIES"]) &&
                                                    !$arResult["PROPERTIES"]["DISCOUNT"]["VALUE"] == false):?>
                                                    <span class="discount">%</span>
                                                <?endif;
                                            endif; ?>
                                        </div>
                                        <? if (!empty($arResult["PROPERTIES"]["MANUFACTURER"]["PREVIEW_IMG"]["SRC"])): ?>
                                            <img class="manufacturer"
                                                 src="<?= $arResult['PROPERTIES']['MANUFACTURER']['PREVIEW_IMG']['SRC'] ?>"
                                                 width="<?= $arResult['PROPERTIES']['MANUFACTURER']['PREVIEW_IMG']['WIDTH'] ?>"
                                                 height="<?= $arResult['PROPERTIES']['MANUFACTURER']['PREVIEW_IMG']['HEIGHT'] ?>"
                                                 alt="<?= $arResult['PROPERTIES']['MANUFACTURER']['NAME'] ?>"/>
                                        <? endif; ?>
                                    </a>
                                </div>
                            <?endforeach;
                        endif;
                    endif;
                    if (
                    !isset($arResult["OFFERS"]) || empty($arResult["OFFERS"]) ||
                    $arSetting["OFFERS_VIEW"]["VALUE"] == "LIST"): ?>
                    <div class="detail_picture">
                        <meta content="<?= is_array($arResult['DETAIL_IMG']) ? $arResult['DETAIL_PICTURE']['SRC'] :
                            SITE_TEMPLATE_PATH . '/images/no-photo.jpg' ?>"
                              itemprop="image"/>
                        <? if (is_array($arResult["DETAIL_IMG"])): ?>
                        <a rel="lightbox" class="catalog-detail-images"
                           href="<?= $arResult['DETAIL_PICTURE']['SRC'] ?>">
                            <img src="<?= $arResult['DETAIL_IMG']['SRC'] ?>"
                                 width="<?= $arResult['DETAIL_IMG']['WIDTH'] ?>"
                                 height="<?= $arResult['DETAIL_IMG']['HEIGHT'] ?>" alt="<?= $arResult['NAME'] ?>"/>
                            <? else: ?>
                            <div class="catalog-detail-images">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/images/no-photo.jpg" width="150" height="150"
                                     alt="<?= $arResult['NAME'] ?>"/>
                                <? endif; ?>
                                <div class="time_buy_sticker">
                                    <?= $timeBuy ?>
                                </div>
                                <div class="sticker">
                                    <?= $sticker ?>
                                </div>
                                <? if (!empty($arResult["PROPERTIES"]["MANUFACTURER"]["PREVIEW_IMG"]["SRC"])): ?>
                                    <img class="manufacturer"
                                         src="<?= $arResult['PROPERTIES']['MANUFACTURER']['PREVIEW_IMG']['SRC'] ?>"
                                         width="<?= $arResult['PROPERTIES']['MANUFACTURER']['PREVIEW_IMG']['WIDTH'] ?>"
                                         height="<?= $arResult['PROPERTIES']['MANUFACTURER']['PREVIEW_IMG']['HEIGHT'] ?>"
                                         alt="<?= $arResult['PROPERTIES']['MANUFACTURER']['NAME'] ?>"/>
                                <? endif; ?>
                                <?= is_array($arResult["DETAIL_IMG"]) ? "</a>" : "</div>"; ?>
                            </div>
                            <? endif; ?>

                            <? /***VIDEO_MORE_PHOTO***/
                            if (!empty($arResult["PROPERTIES"]["VIDEO"]) || count($arResult["MORE_PHOTO"]) > 0): ?>
                                <div class="clr"></div>
                                <div class="more_photo">

                                    <? if (!empty($arResult["PROPERTIES"]["VIDEO"]["VALUE"])): ?>
                                        <ul>
                                            <li class="catalog-detail-video">
                                                <a rel="lightbox" class="catalog-detail-images" href="#video">
                                                    <i class="fa fa-play-circle-o"></i>
                                                    <span><?= GetMessage("CATALOG_ELEMENT_VIDEO") ?></span>
                                                </a>
                                                <div id="video" style="overflow:hidden;">
                                                    <?= $arResult["PROPERTIES"]["VIDEO"]["~VALUE"]["TEXT"]; ?>
                                                </div>
                                            </li>
                                        </ul>
                                    <? endif; ?>

                                    <!--***PICTURE***-->
                                    <? if (isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])):
                                        if ($arSetting["OFFERS_VIEW"]["VALUE"] != "LIST"):?>
                                            <ul>
                                                <? foreach ($arResult["OFFERS"] as $key => $arOffer):
                                                    $singlePicture =
                                                        $arOffer["DISPLAY_PROPERTIES"]["OFFER_MORE_PHOTO"]["FILE_VALUE"];
                                                    if (count($singlePicture['SRC']) > 0):?>
                                                        <li id="offer_picture_<?= $arOffer['ID'] ?>_<?= $key ?>"
                                                            class="offer_picture <?= $arResult['ID'] ?> hidden">
                                                            <a rel="lightbox" class="catalog-detail-images"
                                                               href="<?= $singlePicture['SRC'] ?>">
                                                                <img src="<?= $singlePicture['SRC'] ?>"
                                                                     width="71"
                                                                     alt="<?= $singlePicture['FILE_NAME'] ?>"/>
                                                            </a>
                                                        </li>
                                                    <?
                                                    else:
                                                        foreach ($arOffer["DISPLAY_PROPERTIES"]["OFFER_MORE_PHOTO"]["FILE_VALUE"]
                                                                 as
                                                                 $key => $Picture):?>
                                                            <li id="offer_picture_<?= $arOffer['ID'] ?>_<?= $key ?>"
                                                                class="offer_picture <?= $arResult['ID'] ?> hidden">
                                                                <a rel="lightbox" class="catalog-detail-images"
                                                                   href="<?= $Picture['SRC'] ?>">
                                                                    <img src="<?= $Picture["SRC"] ?>"
                                                                         width="71"
                                                                         alt="<?= $Picture['FILE_NAME'] ?>"/>
                                                                </a>
                                                            </li>
                                                        <?endforeach;
                                                    endif;
                                                endforeach; ?>
                                            </ul>
                                        <? elseif (count($arResult["MORE_PHOTO"]) > 0): ?>
                                            <ul>
                                                <? foreach ($arResult["MORE_PHOTO"] as $PHOTO): ?>
                                                    <li>
                                                        <a rel="lightbox" class="catalog-detail-images"
                                                           href="<?= $PHOTO['SRC'] ?>">
                                                            <img src="<?= $PHOTO['PREVIEW']['SRC'] ?>"
                                                                 width="<?= $PHOTO['PREVIEW']['WIDTH'] ?>"
                                                                 height="<?= $PHOTO['PREVIEW']['HEIGHT'] ?>"
                                                                 alt="<?= $arResult['NAME'] ?>"/>
                                                        </a>
                                                    </li>
                                                <? endforeach; ?>
                                            </ul>
                                        <? endif; ?>
                                    <? endif; ?>
                                </div>
                            <? endif ?>
                    </div>
                </div>
            </div>
            <div class="column second">
                <div class="price_buy_detail" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                    <input type="radio" name="product_price" value="product_price" id="product_price" checked>
                    <label for="product_price">

                        <div class="catalog-detail-price control_price" id="<?= $arItemIDs['PRICE']; ?>">
                            <? /***PRICE***/
                            if (isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])):
                                if ($arSetting["OFFERS_VIEW"]["VALUE"] != "LIST"):
                                    foreach ($arResult["OFFERS"] as $key => $arOffer):?>
                                        <div id="detail_price_<?= $arOffer['ID'] ?>"
                                             class="detail_price <?= $arResult['ID'] ?> hidden">
                                            <? foreach ($arOffer["PRICES"] as $code => $arPrice):
                                                if ($arPrice["MIN_PRICE"] == "Y"):
                                                    if ($arPrice["CAN_ACCESS"]):

                                                        $price =
                                                            CCurrencyLang::GetCurrencyFormat($arPrice["CURRENCY"],
                                                                "ru");
                                                        if (empty($price["THOUSANDS_SEP"])):
                                                            $price["THOUSANDS_SEP"] = " ";
                                                        endif;
                                                        if ($price["HIDE_ZERO"] == "Y"):
                                                            if (
                                                                round($arPrice["DISCOUNT_VALUE"],
                                                                    $price["DECIMALS"]) ==
                                                                round($arPrice["DISCOUNT_VALUE"], 0)):
                                                                $price["DECIMALS"] = 0;
                                                            endif;
                                                        endif;
                                                        $currency = str_replace("#", " ", $price["FORMAT_STRING"]);

                                                        if ($arPrice["DISCOUNT_VALUE"] <= 0):
                                                            $arResult["OFFERS"][$key]["ASK_PRICE"] = 1; ?>
                                                            <span class="catalog-detail-item-no-price">
														<?= GetMessage("CATALOG_ELEMENT_NO_PRICE") ?>
                                                        <?= (!empty($arOffer["CATALOG_MEASURE_NAME"])) ?
                                                            GetMessage("CATALOG_ELEMENT_UNIT") . " " .
                                                            $arOffer["CATALOG_MEASURE_NAME"] : ""; ?>
													</span>
                                                        <?
                                                        else:
                                                            if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
                                                                <span class="catalog-detail-item-price-old">
															<?= $arPrice["PRINT_VALUE"]; ?>
														</span>
                                                                <span class="catalog-detail-item-price-percent">
															<?= GetMessage('CATALOG_ELEMENT_SKIDKA') . " " .
                                                            $arPrice["PRINT_DISCOUNT_DIFF"]; ?>
														</span>
                                                            <? endif; ?>
                                                            <span class="catalog-detail-item-price"
                                                                  data-price="<?= $arPrice['DISCOUNT_VALUE'] ?>">
														<?= number_format($arPrice["DISCOUNT_VALUE"],
                                                            $price["DECIMALS"], $price["DEC_POINT"],
                                                            $price["THOUSANDS_SEP"]); ?>
                                                                <span class="unit">
															<?= $currency ?>
                                                            <?= (!empty($arOffer["CATALOG_MEASURE_NAME"])) ?
                                                                GetMessage("CATALOG_ELEMENT_UNIT") . " " .
                                                                $arOffer["CATALOG_MEASURE_NAME"] : ""; ?>
														</span>
													</span>
                                                        <? endif; ?>
                                                        <meta itemprop="price"
                                                              content="<?= $arPrice['DISCOUNT_VALUE'] ?>"/>
                                                        <meta itemprop="priceCurrency"
                                                              content="<?= $arPrice['CURRENCY'] ?>"/>
                                                    <?endif;
                                                endif;
                                            endforeach; ?>
                                            <?
                                            if (!empty($arResult['PROPERTIES']['DELIVERY_PRICE']["VALUE"])):?>
                                                <div class="delivery-price">
                                                    <?= GetMessage('CATALOG_DELIVERY_PRICE'); ?>
                                                    : <?= CurrencyFormat($arResult['PROPERTIES']['DELIVERY_PRICE']["VALUE"],
                                                        "RUB"); ?>
                                                </div>
                                            <? endif; ?>
                                            <?
                                            if (!empty($arResult['PROPERTIES']['DELIVERY_TIME']["VALUE"])):?>
                                                <div class="delivery-time">
                                                    <?= GetMessage('CATALOG_DELIVERY_TIME'); ?>
                                                    : <?= $arResult['PROPERTIES']['DELIVERY_TIME']["VALUE"] ?>
                                                </div>
                                            <? endif; ?>
                                            <div class="available">
                                                <? /***AVAILABILITY***/
                                                if ($arOffer["CAN_BUY"]):?>
                                                    <meta content="InStock" itemprop="availability"/>
                                                    <div class="avl">
                                                        <i class="fa fa-check-circle"></i>
                                                        <span>
													<?= GetMessage("CATALOG_ELEMENT_AVAILABLE");
                                                    if ($arResult["CATALOG_QUANTITY_TRACE"] == "Y"):
                                                        if (
                                                        in_array("PRODUCT_QUANTITY",
                                                            $arSetting["GENERAL_SETTINGS"]["VALUE"])):
                                                            echo " " . $arOffer["CATALOG_QUANTITY"];
                                                        endif;
                                                    endif; ?>
												</span>
                                                    </div>
                                                <? elseif (!$arOffer["CAN_BUY"]): ?>
                                                    <meta content="OutOfStock" itemprop="availability"/>
                                                    <div class="not_avl">
                                                        <i class="fa fa-times-circle"></i>
                                                        <span><?= GetMessage("CATALOG_ELEMENT_NOT_AVAILABLE") ?></span>
                                                    </div>
                                                <? endif; ?>
                                            </div>
                                        </div>
                                    <?endforeach;
                                elseif ($arSetting["OFFERS_VIEW"]["VALUE"] == "LIST"):?>
                                    <div class="detail_price">
                                        <? $price =
                                            CCurrencyLang::GetCurrencyFormat($arResult["TOTAL_OFFERS"]["MIN_PRICE"]["CURRENCY"],
                                                "ru");
                                        if (empty($price["THOUSANDS_SEP"])):
                                            $price["THOUSANDS_SEP"] = " ";
                                        endif;
                                        if ($price["HIDE_ZERO"] == "Y"):
                                            if (
                                                round($arResult["TOTAL_OFFERS"]["MIN_PRICE"]["DISCOUNT_VALUE"],
                                                    $price["DECIMALS"]) ==
                                                round($arResult["TOTAL_OFFERS"]["MIN_PRICE"]["DISCOUNT_VALUE"], 0)):
                                                $price["DECIMALS"] = 0;
                                            endif;
                                        endif;
                                        $currency = str_replace("#", " ", $price["FORMAT_STRING"]);

                                        if ($arResult["TOTAL_OFFERS"]["MIN_PRICE"]["DISCOUNT_VALUE"] <= 0):?>
                                            <span class="catalog-detail-item-no-price">
										<?= GetMessage("CATALOG_ELEMENT_NO_PRICE") ?>
                                        <?= (!empty($arResult["TOTAL_OFFERS"]["MIN_PRICE"]["CATALOG_MEASURE_NAME"])) ?
                                            GetMessage("CATALOG_ELEMENT_UNIT") . " " .
                                            $arResult["TOTAL_OFFERS"]["MIN_PRICE"]["CATALOG_MEASURE_NAME"] : ""; ?>
									</span>
                                        <?
                                        else:
                                            if (
                                                $arResult["TOTAL_OFFERS"]["MIN_PRICE"]["DISCOUNT_VALUE"] <
                                                $arResult["TOTAL_OFFERS"]["MIN_PRICE"]["VALUE"]):?>
                                                <span class="catalog-detail-item-price-old">
											<?= $arResult["TOTAL_OFFERS"]["MIN_PRICE"]["PRINT_VALUE"]; ?>
										</span>
                                                <span class="catalog-detail-item-price-percent">
											<?= GetMessage('CATALOG_ELEMENT_SKIDKA') . " " .
                                            $arResult["TOTAL_OFFERS"]["MIN_PRICE"]["PRINT_DISCOUNT_DIFF"]; ?>
										</span>
                                            <? endif; ?>
                                            <span class="catalog-detail-item-price">
										<?= ($arResult["TOTAL_OFFERS"]["FROM"] == "Y") ?
                                            "<span class='from'>" . GetMessage("CATALOG_ELEMENT_FROM") . "</span>" :
                                            ""; ?>
                                                <?= number_format($arResult["TOTAL_OFFERS"]["MIN_PRICE"]["DISCOUNT_VALUE"],
                                                    $price["DECIMALS"], $price["DEC_POINT"],
                                                    $price["THOUSANDS_SEP"]); ?>
                                                <span class="unit">
											<?= $currency ?>
                                            <?= (!empty($arResult["TOTAL_OFFERS"]["MIN_PRICE"]["CATALOG_MEASURE_NAME"])) ?
                                                GetMessage("CATALOG_ELEMENT_UNIT") . " " .
                                                $arResult["TOTAL_OFFERS"]["MIN_PRICE"]["CATALOG_MEASURE_NAME"] : ""; ?>
										</span>
									</span>
                                        <? endif; ?>
                                        <meta itemprop="price"
                                              content="<?= $arResult['TOTAL_OFFERS']['MIN_PRICE']['DISCOUNT_VALUE'] ?>"/>
                                        <meta itemprop="priceCurrency"
                                              content="<?= $arResult['TOTAL_OFFERS']['MIN_PRICE']['CURRENCY'] ?>"/>
                                        <?
                                        if (!empty($arResult['PROPERTIES']['DELIVERY_PRICE']["VALUE"])):?>
                                            <div class="delivery-price">
                                                <?= GetMessage('CATALOG_DELIVERY_PRICE'); ?>
                                                : <?= CurrencyFormat($arResult['PROPERTIES']['DELIVERY_PRICE']["VALUE"],
                                                    "RUB"); ?>
                                            </div>
                                        <? endif; ?>
                                        <?
                                        if (!empty($arResult['PROPERTIES']['DELIVERY_TIME']["VALUE"])):?>
                                            <div class="delivery-time">
                                                <?= GetMessage('CATALOG_DELIVERY_TIME'); ?>
                                                : <?= $arResult['PROPERTIES']['DELIVERY_TIME']["VALUE"] ?>
                                            </div>
                                        <? endif; ?>
                                        <div class="available">
                                            <? /***AVAILABILITY***/
                                            if ($arResult["TOTAL_OFFERS"]["QUANTITY"] > 0):?>
                                                <meta content="InStock" itemprop="availability"/>
                                                <div class="avl">
                                                    <i class="fa fa-check-circle"></i>
                                                    <span>
												<?= GetMessage("CATALOG_ELEMENT_AVAILABLE");
                                                if ($arResult["CATALOG_QUANTITY_TRACE"] == "Y"):
                                                    if (
                                                    in_array("PRODUCT_QUANTITY",
                                                        $arSetting["GENERAL_SETTINGS"]["VALUE"])):
                                                        echo " " . $arResult["TOTAL_OFFERS"]["QUANTITY"];
                                                    endif;
                                                endif; ?>
											</span>
                                                </div>
                                            <? else: ?>
                                                <meta content="OutOfStock" itemprop="availability"/>
                                                <div class="not_avl">
                                                    <i class="fa fa-times-circle"></i>
                                                    <span><?= GetMessage("CATALOG_ELEMENT_NOT_AVAILABLE") ?></span>
                                                </div>
                                            <? endif; ?>
                                        </div>
                                    </div>
                                <?endif;
                                /***TIME_BUY_QUANTITY***/
                                if (
                                    array_key_exists("TIME_BUY", $arResult["PROPERTIES"]) &&
                                    !$arResult["PROPERTIES"]["TIME_BUY"]["VALUE"] == false):
                                    if (!empty($arResult["CURRENT_DISCOUNT"]["ACTIVE_TO"])):
                                        if ($arResult["CATALOG_QUANTITY_TRACE"] == "Y"):
                                            $startQnt = $arResult["PROPERTIES"]["TIME_BUY_FROM"]["VALUE"];
                                            $currQnt = $arResult["PROPERTIES"]["TIME_BUY_TO"]["VALUE"] ?
                                                $arResult["PROPERTIES"]["TIME_BUY_TO"]["VALUE"] :
                                                $arResult["TOTAL_OFFERS"]["QUANTITY"];
                                            $currQntPercent = round($currQnt * 100 / $startQnt);
                                        else:
                                            $currQntPercent = 100;
                                        endif; ?>

                                        <div class="progress_bar_block">
                                            <span class="progress_bar_title"><?= GetMessage("CATALOG_ELEMENT_QUANTITY_PERCENT") ?></span>
                                            <div class="progress_bar_cont">
                                                <div class="progress_bar_bg">
                                                    <div class="progress_bar_line"
                                                         style="width:<?= $currQntPercent ?>%;"></div>
                                                </div>
                                            </div>
                                            <span class="progress_bar_percent"><?= $currQntPercent ?>%</span>
                                        </div>
                                    <?endif;
                                endif;
                            else:
                                foreach ($arResult["PRICES"] as $code => $arPrice):
                                    if ($arPrice["MIN_PRICE"] == "Y"):
                                        if ($arPrice["CAN_ACCESS"]):

                                            $price = CCurrencyLang::GetCurrencyFormat($arPrice["CURRENCY"], "ru");
                                            if (empty($price["THOUSANDS_SEP"])):
                                                $price["THOUSANDS_SEP"] = " ";
                                            endif;
                                            if ($price["HIDE_ZERO"] == "Y"):
                                                if (
                                                    round($arPrice["DISCOUNT_VALUE"], $price["DECIMALS"]) ==
                                                    round($arPrice["DISCOUNT_VALUE"], 0)):
                                                    $price["DECIMALS"] = 0;
                                                endif;
                                            endif;
                                            $currency = str_replace("#", " ", $price["FORMAT_STRING"]);

                                            if ($arPrice["DISCOUNT_VALUE"] <= 0):
                                                $arResult["ASK_PRICE"] = 1; ?>
                                                <span class="catalog-detail-item-no-price">
											<?= GetMessage("CATALOG_ELEMENT_NO_PRICE") ?>
                                            <?= (!empty($arResult["CATALOG_MEASURE_NAME"])) ?
                                                GetMessage("CATALOG_ELEMENT_UNIT") . " " .
                                                $arResult["CATALOG_MEASURE_NAME"] : ""; ?>
										</span>
                                            <?
                                            else:
                                                if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
                                                    <span class="catalog-detail-item-price-old">
												<?= $arPrice["PRINT_VALUE"]; ?>
											</span>
                                                    <span class="catalog-detail-item-price-percent">
												<?= GetMessage('CATALOG_ELEMENT_SKIDKA') . " " .
                                                $arPrice["PRINT_DISCOUNT_DIFF"]; ?>
											</span>
                                                <? endif; ?>
                                                <span class="catalog-detail-item-price">
											<?= number_format($arPrice["DISCOUNT_VALUE"], $price["DECIMALS"],
                                                $price["DEC_POINT"], $price["THOUSANDS_SEP"]); ?>
                                                    <span class="unit">
												<?= $currency ?>
                                                <?= (!empty($arResult["CATALOG_MEASURE_NAME"])) ?
                                                    GetMessage("CATALOG_ELEMENT_UNIT") . " " .
                                                    $arResult["CATALOG_MEASURE_NAME"] : ""; ?>
											</span>
										</span>
                                            <? endif; ?>
                                            <meta itemprop="price" content="<?= $arPrice['DISCOUNT_VALUE'] ?>"/>
                                            <meta itemprop="priceCurrency" content="<?= $arPrice['CURRENCY'] ?>"/>
                                        <?endif;
                                    endif;
                                endforeach; ?>
                                <? if (!empty($arResult['PROPERTIES']['DELIVERY_PRICE']["VALUE"])): ?>
                                <div class="delivery-price">
                                    <?= GetMessage('CATALOG_DELIVERY_PRICE'); ?>
                                    : <?= CurrencyFormat($arResult['PROPERTIES']['DELIVERY_PRICE']["VALUE"],
                                        "RUB"); ?>
                                </div>
                            <? endif; ?>
                                <?
                                if (!empty($arResult['PROPERTIES']['DELIVERY_TIME']["VALUE"])):?>
                                    <div class="delivery-time">
                                        <?= GetMessage('CATALOG_DELIVERY_TIME'); ?>
                                        : <?= $arResult['PROPERTIES']['DELIVERY_TIME']["VALUE"] ?>
                                    </div>
                                <? endif; ?>
                                <div class="available">
                                    <? /***AVAILABILITY***/
                                    if ($arResult["CAN_BUY"]):?>
                                        <meta content="InStock" itemprop="availability"/>
                                        <div class="avl">
                                            <i class="fa fa-check-circle"></i>
                                            <span>
										<?= GetMessage("CATALOG_ELEMENT_AVAILABLE");
                                        if ($arResult["CATALOG_QUANTITY_TRACE"] == "Y"):
                                            if (in_array("PRODUCT_QUANTITY", $arSetting["GENERAL_SETTINGS"]["VALUE"])):
                                                echo " " . $arResult["CATALOG_QUANTITY"];
                                            endif;
                                        endif; ?>
									</span>
                                        </div>
                                    <? elseif (!$arResult["CAN_BUY"]): ?>
                                        <meta content="OutOfStock" itemprop="availability"/>
                                        <div class="not_avl">
                                            <i class="fa fa-times-circle"></i>
                                            <span><?= GetMessage("CATALOG_ELEMENT_NOT_AVAILABLE") ?></span>
                                        </div>
                                    <? endif; ?>
                                </div>
                                <? /***TIME_BUY_QUANTITY***/
                                if (
                                    array_key_exists("TIME_BUY", $arResult["PROPERTIES"]) &&
                                    !$arResult["PROPERTIES"]["TIME_BUY"]["VALUE"] == false):
                                    if (!empty($arResult["CURRENT_DISCOUNT"]["ACTIVE_TO"])):
                                        if ($arResult["CAN_BUY"]):
                                            if ($arResult["CATALOG_QUANTITY_TRACE"] == "Y"):
                                                $startQnt = $arResult["PROPERTIES"]["TIME_BUY_FROM"]["VALUE"];
                                                $currQnt = $arResult["PROPERTIES"]["TIME_BUY_TO"]["VALUE"] ?
                                                    $arResult["PROPERTIES"]["TIME_BUY_TO"]["VALUE"] :
                                                    $arResult["CATALOG_QUANTITY"];
                                                $currQntPercent = round($currQnt * 100 / $startQnt);
                                            else:
                                                $currQntPercent = 100;
                                            endif; ?>

                                            <div class="progress_bar_block">
                                                <span class="progress_bar_title"><?= GetMessage("CATALOG_ELEMENT_QUANTITY_PERCENT") ?></span>
                                                <div class="progress_bar_cont">
                                                    <div class="progress_bar_bg">
                                                        <div class="progress_bar_line"
                                                             style="width:<?= $currQntPercent ?>%;"></div>
                                                    </div>
                                                </div>
                                                <span class="progress_bar_percent"><?= $currQntPercent ?>%</span>
                                            </div>
                                        <?endif;
                                    endif;
                                endif;
                            endif; ?>
                        </div>
                    </label>

                    <input type="radio" name="product_price" value="set_price" id="set_price">
                    <label for="set_price" class="product-set" id="product-set">
                        <div class="ui-button-text">
                            <span class="price" id="product-set-price"></span>
                            <span class="rub"><?= $currency ?> <?= GetMessage("CATALOG_ELEMENT_FOR_COMPLECT") ?></span>
                            <div class="product-set_detail">
                                        <span class="pr-text js-short-complect"
                                              data-complect="complect"><span><?= GetMessage("CATALOG_ELEMENT_SET") ?></span>
                                    </span>
                            </div>
                        </div>
                    </label>
                    <div class="buy-wrapper">
                        <div class="catalog-detail-buy" id="<?= $arItemIDs['BUY']; ?>">
                            <? /***BUY***/
                            if (isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])):
                                /***TIME_BUY_TIMER***/
                            if (array_key_exists("TIME_BUY", $arResult["PROPERTIES"]) &&
                                !$arResult["PROPERTIES"]["TIME_BUY"]["VALUE"] == false):
                            if (!empty($arResult["CURRENT_DISCOUNT"]["ACTIVE_TO"])):
                                $new_date =
                                    ParseDateTime($arResult["CURRENT_DISCOUNT"]["ACTIVE_TO"], FORMAT_DATETIME); ?>
                                <script type="text/javascript">
                                    $(function () {
                                        $("#time_buy_timer").countdown({
                                            until: new Date(<?=$new_date["YYYY"]?>, <?=$new_date["MM"]?> -1, <?=$new_date["DD"]?>, <?=$new_date["HH"]?>, <?=$new_date["MI"]?>),
                                            format: "DHMS",
                                            expiryText: "<div class='over'><?=GetMessage('CATALOG_ELEMENT_TIME_BUY_EXPIRY')?></div>"
                                        });
                                    });
                                </script>
                                <div class="time_buy_cont">
                                    <div class="time_buy_clock">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <div class="time_buy_timer" id="time_buy_timer"></div>
                                </div>
                            <?
                            endif;
                            endif;
                            if ($arSetting["OFFERS_VIEW"]["VALUE"] != "LIST"):
                            foreach ($arResult["OFFERS"] as $key => $arOffer): ?>
                                <div id="buy_more_detail_<?= $arOffer['ID'] ?>"
                                     class="buy_more_detail <?= $arResult['ID'] ?> hidden">
                                    <? if ($arOffer["CAN_BUY"]):
                                        if ($arOffer["ASK_PRICE"]):?>
                                            <a class="btn_buy apuo_detail" id="ask_price_anch_<?= $arOffer['ID'] ?>"
                                               href="javascript:void(0)" rel="nofollow"><i
                                                        class="fa fa-comment-o"></i><?= GetMessage("CATALOG_ELEMENT_ASK_PRICE") ?>
                                            </a>
                                            <? $properties = false;
                                            foreach ($arOffer["DISPLAY_PROPERTIES"] as $propOffer) {
                                                $properties[] =
                                                    $propOffer["NAME"] . ": " . strip_tags($propOffer["DISPLAY_VALUE"]);
                                            }
                                            $properties = implode("; ", $properties);
                                            if (!empty($properties)):
                                                $offer_name = $arResult["NAME"] . " (" . $properties . ")";
                                            else:
                                                $offer_name = $arResult["NAME"];
                                            endif; ?>
                                            <? $APPLICATION->IncludeComponent("altop:ask.price", "",
                                                Array(
                                                    "ELEMENT_ID" => $arOffer["ID"],
                                                    "ELEMENT_NAME" => $offer_name,
                                                    "EMAIL_TO" => "",
                                                    "REQUIRED_FIELDS" => array("NAME", "TEL", "TIME")
                                                ),
                                                false,
                                                array("HIDE_ICONS" => "Y")
                                            ); ?>
                                        <? elseif (!$arOffer["ASK_PRICE"]): ?>

                                            <div class="add2basket_block">
                                                <form action="<?= SITE_DIR ?>ajax/add2basket.php"
                                                      class="add2basket_form"
                                                      id="add2basket_form_<?= $arOffer['ID'] ?>">


                                                    <div class="qnt_cont">
                                                        <a href="javascript:void(0)" class="minus"
                                                           onclick="if (BX('quantity_<?= $arOffer["ID"] ?>').value > <?= $arOffer["CATALOG_MEASURE_RATIO"] ?>) BX('quantity_<?= $arOffer["ID"] ?>').value = parseFloat(BX('quantity_<?= $arOffer["ID"] ?>').value)-<?= $arOffer["CATALOG_MEASURE_RATIO"] ?>;"><span>-</span></a>
                                                        <input type="text" id="quantity_<?= $arOffer['ID'] ?>"
                                                               name="quantity" class="quantity"
                                                               value="<?= $arOffer['CATALOG_MEASURE_RATIO'] ?>"/>
                                                        <a href="javascript:void(0)" class="plus"
                                                           onclick="BX('quantity_<?= $arOffer["ID"] ?>').value = parseFloat(BX('quantity_<?= $arOffer["ID"] ?>').value)+<?= $arOffer["CATALOG_MEASURE_RATIO"] ?>;"><span>+</span></a>
                                                    </div>

                                                    <input type="hidden" name="ID" class="offer_id"
                                                           value="<?= $arOffer['ID'] ?>"/>
                                                    <? $props = array();
                                                    foreach ($arOffer["DISPLAY_PROPERTIES"] as $propOffer) {
                                                        $props[] = array(
                                                            "NAME" => $propOffer["NAME"],
                                                            "CODE" => $propOffer["CODE"],
                                                            "VALUE" => strip_tags($propOffer["DISPLAY_VALUE"])
                                                        );
                                                    }
                                                    $props =
                                                        strtr(base64_encode(addslashes(gzcompress(serialize($props),
                                                            9))),
                                                            '+/=', '-_,'); ?>
                                                    <input type="hidden" name="PROPS" value="<?= $props ?>"/>
                                                    <? if (!empty($arResult["SELECT_PROPS"])): ?>
                                                        <input type="hidden" name="SELECT_PROPS"
                                                               id="select_props_<?= $arOffer['ID'] ?>" value=""/>
                                                    <? endif; ?>
                                                    <? if (!empty($arOffer["PREVIEW_IMG"]["SRC"])): ?>
                                                        <input type="hidden" name="item_image" class="item_image"
                                                               value="&lt;img class='item_image' src='<?= $arOffer["PREVIEW_IMG"]["SRC"] ?>' alt='<?= $arResult["NAME"] ?>'/&gt;"/>
                                                    <? else: ?>
                                                        <input type="hidden" name="item_image" class="item_image"
                                                               value="&lt;img class='item_image' src='<?= $arResult["PREVIEW_IMG"]["SRC"] ?>' alt='<?= $arResult["NAME"] ?>'/&gt;"/>
                                                    <? endif; ?>
                                                    <input type="hidden" name="item_title" class="item_title"
                                                           value="<?= $arResult['NAME'] ?>"/>
                                                    <input type="hidden" name="item_props" class="item_props" value="
														<? foreach ($arOffer["DISPLAY_PROPERTIES"] as $propOffer):
                                                        echo '&lt;span&gt;' . $propOffer["NAME"] . ': ' .
                                                            strip_tags($propOffer["DISPLAY_VALUE"]) . '&lt;/span&gt;';
                                                    endforeach; ?>
													"/>
                                                    <button type="submit" name="add2basket" class="btn_buy detail"
                                                            value="<?= GetMessage('CATALOG_ELEMENT_ADD_TO_CART') ?>"><i
                                                                class="fa fa-shopping-cart"></i><?= GetMessage('CATALOG_ELEMENT_ADD_TO_CART') ?>
                                                    </button>
                                                    <small class="result detail hidden"><i
                                                                class="fa fa-check"></i><?= GetMessage('CATALOG_ELEMENT_ADDED') ?>
                                                    </small>
                                                </form>
                                                <button name="boc_anch" id="boc_anch_<?= $arOffer['ID'] ?>"
                                                        class="btn_buy boc_anch"
                                                        value="<?= GetMessage('CATALOG_ELEMENT_BOC') ?>"><i
                                                            class="fa fa-bolt"></i><?= GetMessage('CATALOG_ELEMENT_BOC') ?>
                                                </button>
                                                <? $APPLICATION->IncludeComponent("altop:buy.one.click", ".default",
                                                    array(
                                                        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                                                        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                                                        "ELEMENT_ID" => $arOffer["ID"],
                                                        "ELEMENT_CODE" => "",
                                                        "ELEMENT_PROPS" => $props,
                                                        "REQUIRED_ORDER_FIELDS" => array(
                                                            0 => "NAME",
                                                            1 => "TEL",
                                                        ),
                                                        "DEFAULT_PERSON_TYPE" => "1",
                                                        "DEFAULT_ORDER_PROP_NAME" => "1",
                                                        "DEFAULT_ORDER_PROP_TEL" => "3",
                                                        "DEFAULT_ORDER_PROP_EMAIL" => "2",
                                                        "DEFAULT_DELIVERY" => "0",
                                                        "DEFAULT_PAYMENT" => "0",
                                                        "BUY_MODE" => "ONE",
                                                        "DUPLICATE_LETTER_TO_EMAILS" => array(
                                                            0 => "admin",
                                                        ),
                                                    ),
                                                    false,
                                                    array("HIDE_ICONS" => "Y")
                                                ); ?>
                                            </div>
                                        <?endif;
                                    elseif (!$arOffer["CAN_BUY"]):?>
                                        <a class="btn_buy apuo_detail" id="order_anch_<?= $arOffer['ID'] ?>"
                                           href="javascript:void(0)" rel="nofollow"><i
                                                    class="fa fa-clock-o"></i><?= GetMessage("CATALOG_ELEMENT_UNDER_ORDER") ?>
                                        </a>
                                        <? $properties = false;
                                        foreach ($arOffer["DISPLAY_PROPERTIES"] as $propOffer) {
                                            $properties[] =
                                                $propOffer["NAME"] . ": " . strip_tags($propOffer["DISPLAY_VALUE"]);
                                        }
                                        $properties = implode("; ", $properties);
                                        if (!empty($properties)):
                                            $offer_name = $arResult["NAME"] . " (" . $properties . ")";
                                        else:
                                            $offer_name = $arResult["NAME"];
                                        endif; ?>
                                        <? $APPLICATION->IncludeComponent("altop:ask.price", "order",
                                            Array(
                                                "ELEMENT_ID" => $arOffer["ID"],
                                                "ELEMENT_NAME" => $offer_name,
                                                "EMAIL_TO" => "",
                                                "REQUIRED_FIELDS" => array("NAME", "TEL", "TIME")
                                            ),
                                            false,
                                            array("HIDE_ICONS" => "Y")
                                        ); ?>
                                        <? $APPLICATION->IncludeComponent("bitrix:sale.notice.product", "",
                                            array(
                                                "NOTIFY_ID" => $arOffer["ID"],
                                                "NOTIFY_URL" => htmlspecialcharsback($arOffer["SUBSCRIBE_URL"]),
                                                "NOTIFY_USE_CAPTHA" => "Y"
                                            ),
                                            false,
                                            array("HIDE_ICONS" => "Y")
                                        ); ?>
                                    <? endif; ?>
                                </div>
                            <?
                            endforeach;
                            elseif ($arSetting["OFFERS_VIEW"]["VALUE"] == "LIST"):?>
                                <div class="buy_more_detail">
                                    <script type="text/javascript">
                                        $(function () {
                                            $("button[name=choose_offer]").click(function () {
                                                var destination = $("#catalog-detail-offers-list").offset().top;
                                                $("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 500);
                                                return false;
                                            });
                                        });
                                    </script>
                                    <button name="choose_offer" class="btn_buy detail"
                                            value="<?= GetMessage('CATALOG_ELEMENT_CHOOSE_OFFER') ?>"><?= GetMessage('CATALOG_ELEMENT_CHOOSE_OFFER') ?></button>
                                </div>
                            <?
                            endif;
                            else:
                            /***TIME_BUY_TIMER***/
                            if (array_key_exists("TIME_BUY", $arResult["PROPERTIES"]) &&
                            !$arResult["PROPERTIES"]["TIME_BUY"]["VALUE"] == false):
                            if (!empty($arResult["CURRENT_DISCOUNT"]["ACTIVE_TO"])):
                            if ($arResult["CAN_BUY"]):
                            $new_date = ParseDateTime($arResult["CURRENT_DISCOUNT"]["ACTIVE_TO"], FORMAT_DATETIME); ?>
                                <script type="text/javascript">
                                    $(function () {
                                        $("#time_buy_timer").countdown({
                                            until: new Date(<?=$new_date["YYYY"]?>, <?=$new_date["MM"]?> -1, <?=$new_date["DD"]?>, <?=$new_date["HH"]?>, <?=$new_date["MI"]?>),
                                            format: "DHMS",
                                            expiryText: "<div class='over'><?=GetMessage('CATALOG_ELEMENT_TIME_BUY_EXPIRY')?></div>"
                                        });
                                    });
                                </script>
                                <div class="time_buy_cont">
                                    <div class="time_buy_clock">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <div class="time_buy_timer" id="time_buy_timer"></div>
                                </div>
                            <?
                            endif;
                            endif;
                            endif;
                            ?>
                                <div class="buy_more_detail">
                                    <? if ($arResult["CAN_BUY"]):
                                        if ($arResult["ASK_PRICE"]):?>
                                            <a class="btn_buy apuo_detail" id="ask_price_anch_<?= $arResult['ID'] ?>"
                                               href="javascript:void(0)" rel="nofollow"><i
                                                        class="fa fa-comment-o"></i><?= GetMessage("CATALOG_ELEMENT_ASK_PRICE") ?>
                                            </a>
                                            <? $APPLICATION->IncludeComponent("altop:ask.price", "",
                                                Array(
                                                    "ELEMENT_ID" => $arResult["ID"],
                                                    "ELEMENT_NAME" => $arResult["NAME"],
                                                    "EMAIL_TO" => "",
                                                    "REQUIRED_FIELDS" => array("NAME", "TEL", "TIME")
                                                ),
                                                false
                                            ); ?>
                                        <? elseif (!$arResult["ASK_PRICE"]): ?>
                                            <form action="<?= SITE_DIR ?>ajax/add2basket.php" class="add2basket_form"
                                                  id="add2basket_form_<?= $arResult['ID'] ?>">
                                                <div class="qnt_cont">
                                                    <a href="javascript:void(0)" class="minus"
                                                       onclick="if(BX('quantity_<?= $arResult["ID"] ?>').value > <?= $arResult["CATALOG_MEASURE_RATIO"] ?>) BX('quantity_<?= $arResult["ID"] ?>').value = parseFloat(BX('quantity_<?= $arResult["ID"] ?>').value)-<?= $arResult["CATALOG_MEASURE_RATIO"] ?>;"><span>-</span></a>
                                                    <input type="text" id="quantity_<?= $arResult['ID'] ?>"
                                                           name="quantity"
                                                           class="quantity"
                                                           value="<?= $arResult['CATALOG_MEASURE_RATIO'] ?>"/>
                                                    <a href="javascript:void(0)" class="plus"
                                                       onclick="BX('quantity_<?= $arResult["ID"] ?>').value = parseFloat(BX('quantity_<?= $arResult["ID"] ?>').value)+<?= $arResult["CATALOG_MEASURE_RATIO"] ?>;"><span>+</span></a>
                                                </div>
                                                <input type="hidden" name="ID" class="id"
                                                       value="<?= $arResult['ID'] ?>"/>
                                                <? if (!empty($arResult["SELECT_PROPS"])): ?>
                                                    <input type="hidden" name="SELECT_PROPS"
                                                           id="select_props_<?= $arResult['ID'] ?>" value=""/>
                                                <? endif; ?>
                                                <input type="hidden" name="item_image" class="item_image"
                                                       value="&lt;img class='item_image' src='<?= $arResult["PREVIEW_IMG"]["SRC"] ?>' alt='<?= $arResult["NAME"] ?>'/&gt;"/>
                                                <input type="hidden" name="item_title" class="item_title"
                                                       value="<?= $arResult['NAME'] ?>"/>
                                                <button type="submit" name="add2basket" class="btn_buy detail"
                                                        value="<?= GetMessage('CATALOG_ELEMENT_ADD_TO_CART') ?>"><i
                                                            class="fa fa-shopping-cart"></i><?= GetMessage('CATALOG_ELEMENT_ADD_TO_CART') ?>
                                                </button>
                                                <small class="result detail hidden"><i
                                                            class="fa fa-check"></i><?= GetMessage('CATALOG_ELEMENT_ADDED') ?>
                                                </small>
                                            </form>
                                            <button name="boc_anch" id="boc_anch_<?= $arResult['ID'] ?>"
                                                    class="btn_buy boc_anch"
                                                    value="<?= GetMessage('CATALOG_ELEMENT_BOC') ?>"><i
                                                        class="fa fa-bolt"></i><?= GetMessage('CATALOG_ELEMENT_BOC') ?>
                                            </button>
                                            <? $APPLICATION->IncludeComponent("altop:buy.one.click", ".default",
                                                array(
                                                    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                                                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                                                    "ELEMENT_ID" => $arResult["ID"],
                                                    "ELEMENT_CODE" => "",
                                                    "ELEMENT_PROPS" => "",
                                                    "REQUIRED_ORDER_FIELDS" => array(
                                                        0 => "NAME",
                                                        1 => "TEL",
                                                    ),
                                                    "DEFAULT_PERSON_TYPE" => "1",
                                                    "DEFAULT_ORDER_PROP_NAME" => "1",
                                                    "DEFAULT_ORDER_PROP_TEL" => "3",
                                                    "DEFAULT_ORDER_PROP_EMAIL" => "2",
                                                    "DEFAULT_DELIVERY" => "0",
                                                    "DEFAULT_PAYMENT" => "0",
                                                    "BUY_MODE" => "ONE",
                                                    "DUPLICATE_LETTER_TO_EMAILS" => array(
                                                        0 => "admin",
                                                    ),
                                                ),
                                                false
                                            ); ?>
                                        <?endif;
                                    elseif (!$arResult["CAN_BUY"]):?>
                                        <a class="btn_buy apuo_detail" id="order_anch_<?= $arResult['ID'] ?>"
                                           href="javascript:void(0)" rel="nofollow"><i
                                                    class="fa fa-clock-o"></i><?= GetMessage("CATALOG_ELEMENT_UNDER_ORDER") ?>
                                        </a>
                                        <? $APPLICATION->IncludeComponent("altop:ask.price", "order",
                                            Array(
                                                "ELEMENT_ID" => $arResult["ID"],
                                                "ELEMENT_NAME" => $arResult["NAME"],
                                                "EMAIL_TO" => "",
                                                "REQUIRED_FIELDS" => array("NAME", "TEL", "TIME")
                                            ),
                                            false
                                        ); ?>
                                        <? $APPLICATION->IncludeComponent("bitrix:sale.notice.product", "",
                                            array(
                                                "NOTIFY_ID" => $arResult["ID"],
                                                "NOTIFY_URL" => htmlspecialcharsback($arResult["SUBSCRIBE_URL"]),
                                                "NOTIFY_USE_CAPTHA" => "Y"
                                            ),
                                            false
                                        ); ?>
                                    <? endif; ?>
                                </div>
                            <? endif; ?>
                        </div>
                        <div class="compare_delay">
                            <? /***COMPARE***/
                            if ($arParams["USE_COMPARE"] == "Y"):?>
                                <div class="compare">
                                    <a href="javascript:void(0)" class="catalog-item-compare"
                                       id="catalog_add2compare_link_<?= $arResult['ID'] ?>"
                                       onclick="return addToCompare('<?= $arResult["COMPARE_URL"] ?>', 'catalog_add2compare_link_<?= $arResult["ID"] ?>');"
                                       rel="nofollow"><span class="compare_cont"><i class="fa fa-bar-chart"></i><i
                                                    class="fa fa-check"></i><span
                                                    class="compare_text"><?= GetMessage('CATALOG_ELEMENT_ADD_TO_COMPARE') ?></span></span></a>
                                </div>
                            <? endif; ?>
                            <div class="catalog-detail-delay" id="<?= $arItemIDs['DELAY'] ?>">
                                <? /***DELAY***/
                                if (isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])):
                                    if ($arSetting["OFFERS_VIEW"]["VALUE"] != "LIST"):
                                        foreach ($arResult["OFFERS"] as $key => $arOffer):
                                            if ($arOffer["CAN_BUY"]):
                                                foreach ($arOffer["PRICES"] as $code => $arPrice):
                                                    if ($arPrice["MIN_PRICE"] == "Y"):
                                                        if ($arPrice["DISCOUNT_VALUE"] > 0):
                                                            $props = array();
                                                            foreach ($arOffer["DISPLAY_PROPERTIES"] as $propOffer) {
                                                                $props[] = array(
                                                                    "NAME" => $propOffer["NAME"],
                                                                    "CODE" => $propOffer["CODE"],
                                                                    "VALUE" => strip_tags($propOffer["DISPLAY_VALUE"])
                                                                );
                                                            }
                                                            $props =
                                                                strtr(base64_encode(addslashes(gzcompress(serialize($props),
                                                                    9))), '+/=', '-_,'); ?>
                                                            <div id="delay_<?= $arOffer['ID'] ?>"
                                                                 class="delay <?= $arResult['ID'] ?> hidden">
                                                                <a href="javascript:void(0)"
                                                                   id="catalog-item-delay-<?= $arOffer['ID'] ?>"
                                                                   class="catalog-item-delay"
                                                                   onclick="return addToDelay('<?= $arOffer["ID"] ?>', '<?= $arOffer["CATALOG_MEASURE_RATIO"] ?>', '<?= $props ?>', '', 'catalog-item-delay-<?= $arOffer["ID"] ?>')"
                                                                   rel="nofollow"><span class="delay_cont"><i
                                                                                class="fa fa-heart-o"></i><i
                                                                                class="fa fa-check"></i><span
                                                                                class="delay_text"><?= GetMessage('CATALOG_ELEMENT_ADD_TO_DELAY') ?></span></span></a>
                                                            </div>
                                                        <?endif;
                                                    endif;
                                                endforeach;
                                            endif;
                                        endforeach;
                                    endif;
                                else:
                                    if ($arResult["CAN_BUY"]):
                                        foreach ($arResult["PRICES"] as $code => $arPrice):
                                            if ($arPrice["MIN_PRICE"] == "Y"):
                                                if ($arPrice["DISCOUNT_VALUE"] > 0):?>
                                                    <div class="delay">
                                                        <a href="javascript:void(0)"
                                                           id="catalog-item-delay-<?= $arResult['ID'] ?>"
                                                           class="catalog-item-delay"
                                                           onclick="return addToDelay('<?= $arResult["ID"] ?>', '<?= $arResult["CATALOG_MEASURE_RATIO"] ?>', '', '', 'catalog-item-delay-<?= $arResult["ID"] ?>')"
                                                           rel="nofollow"><span class="delay_cont"><i
                                                                        class="fa fa-heart-o"></i><i
                                                                        class="fa fa-check"></i><span
                                                                        class="delay_text"><?= GetMessage('CATALOG_ELEMENT_ADD_TO_DELAY') ?></span></span></a>
                                                    </div>
                                                <?endif;
                                            endif;
                                        endforeach;
                                    endif;
                                endif ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="article_rating">
                    <div class="article">
                        <?= GetMessage("CATALOG_ELEMENT_ARTNUMBER") ?>

                        <? if (isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])): ?>
                            <ul class="offer-article-list">
                                <? foreach ($arResult["OFFERS"] as $key => $arOffer):
                                    $offerArticle = $arOffer["DISPLAY_PROPERTIES"]["ARTNUMBER"]["VALUE"];
                                    if (!empty($offerArticle)):?>
                                        <li class="offer_article offer_article_<?= $arOffer['ID'] ?> <?= $arResult['ID'] ?> hidden">
                                            <?= $offerArticle ?>
                                        </li>
                                    <? else : ?>
                                        <li class="offer_article offer_article_<?= $arOffer['ID'] ?> <?= $arResult['ID'] ?> hidden">
                                            -
                                        </li>
                                    <?endif;
                                endforeach; ?>
                            </ul>

                        <? else : ?>
                            <?= !empty($arResult["PROPERTIES"]["ARTNUMBER"]["VALUE"]) ?
                                $arResult["PROPERTIES"]["ARTNUMBER"]["VALUE"] : "-"; ?>
                        <? endif; ?>

                    </div>
                    <div class="rating" itemprop="aggregateRating" itemscope
                         itemtype="http://schema.org/AggregateRating">
                        <? $APPLICATION->IncludeComponent("bitrix:iblock.vote", "ajax",
                            Array(
                                "DISPLAY_AS_RATING" => "vote_avg",
                                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                                "ELEMENT_ID" => $arResult["ID"],
                                "ELEMENT_CODE" => "",
                                "MAX_VOTE" => "5",
                                "VOTE_NAMES" => array("1", "2", "3", "4", "5"),
                                "SET_STATUS_404" => "N",
                                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                                "CACHE_TIME" => $arParams["CACHE_TIME"],
                                "CACHE_NOTES" => "",
                                "READ_ONLY" => "N"
                            ),
                            false,
                            array("HIDE_ICONS" => "Y")
                        ); ?>
                        <? if ($arResult["PROPERTIES"]["vote_count"]["VALUE"]): ?>
                            <meta content="<?= round($arResult['PROPERTIES']['vote_sum']['VALUE'] /
                                $arResult['PROPERTIES']['vote_count']['VALUE'], 2); ?>"
                                  itemprop="ratingValue"/>
                            <meta content="<?= $arResult['PROPERTIES']['vote_count']['VALUE'] ?>"
                                  itemprop="ratingCount"/>
                        <? else: ?>
                            <meta content="0" itemprop="ratingValue"/>
                            <meta content="0" itemprop="ratingCount"/>
                        <? endif; ?>
                    </div>
                </div>

                <? if (!empty($arResult["PREVIEW_TEXT"])): ?>
                    <div class="catalog-detail-preview-text" itemprop="description">
                        <?= $arResult["PREVIEW_TEXT"] ?>
                    </div>
                <?endif;

                /***OFFERS_PROPS***/
                if (isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])):
                    if ($arSetting["OFFERS_VIEW"]["VALUE"] != "LIST"):
                        $arSkuProps = array(); ?>
                        <div class="catalog-detail-offers" id="<?= $arItemIDs['PROP_DIV']; ?>">
                            <? foreach ($arResult["SKU_PROPS"] as &$arProp) {
                                if (!isset($arResult["OFFERS_PROP"][$arProp["CODE"]]))
                                    continue;
                                $arSkuProps[] = array(
                                    "ID" => $arProp["ID"],
                                    "SHOW_MODE" => $arProp["SHOW_MODE"]
                                ); ?>
                                <div class="offer_block" id="<?= $arItemIDs['PROP'] . $arProp['ID']; ?>_cont">
                                    <div class="h3"><?= htmlspecialcharsex($arProp["NAME"]); ?></div>
                                    <ul id="<?= $arItemIDs['PROP'] . $arProp['ID']; ?>_list"
                                        class="<?= $arProp['CODE'] ?><?= $arProp['SHOW_MODE'] == 'PICT' ? ' COLOR' :
                                            ''; ?>">
                                        <? foreach ($arProp["VALUES"] as $arOneValue) {
                                            $arOneValue["NAME"] = htmlspecialcharsbx($arOneValue["NAME"]); ?>
                                            <li data-treevalue="<?= $arProp['ID'] . '_' . $arOneValue['ID']; ?>"
                                                data-onevalue="<?= $arOneValue['ID']; ?>" style="display:none;">
											<span title="<?= $arOneValue['NAME']; ?>">
                                                <? if ("TEXT" == $arProp["SHOW_MODE"]) {
                                                    echo $arOneValue["NAME"];
                                                } elseif ("PICT" == $arProp["SHOW_MODE"]) {
                                                    if (!empty($arOneValue["PICT"]["SRC"])):?>
                                                        <img src="<?= $arOneValue['PICT']['SRC'] ?>" width="24"
                                                             height="24" alt="<?= $arOneValue['NAME'] ?>"/>
                                                    <? else: ?>
                                                        <i style="background:#<?= $arOneValue['HEX'] ?>"></i>
                                                    <?endif;
                                                } ?>
											</span>
                                            </li>
                                            <?
                                        } ?>
                                    </ul>
                                    <div class="bx_slide_left" style="display:none;"
                                         id="<?= $arItemIDs['PROP'] . $arProp['ID'] ?>_left"
                                         data-treevalue="<?= $arProp['ID'] ?>"></div>
                                    <div class="bx_slide_right" style="display:none;"
                                         id="<?= $arItemIDs['PROP'] . $arProp['ID'] ?>_right"
                                         data-treevalue="<?= $arProp['ID'] ?>"></div>
                                </div>
                                <?
                            }
                            unset($arProp); ?>
                        </div>
                    <?endif;
                endif;

                /***SELECT_PROPS***/
                if (isset($arResult["SELECT_PROPS"]) && !empty($arResult["SELECT_PROPS"])):
                    $arSelProps = array(); ?>
                    <div class="catalog-detail-offers" id="<?= $arItemIDs['SELECT_PROP_DIV']; ?>">
                        <? foreach ($arResult["SELECT_PROPS"] as $key => &$arProp):
//                            if(!$arProp["CODE"]["DELIVERY_PRICE"]&& !$arProp["CODE"]["DELIVERY_TIME"]):
                            $arSelProps[] = array(
                                "ID" => $arProp["ID"]
                            ); ?>
                            <div class="offer_block" id="<?= $arItemIDs['SELECT_PROP'] . $arProp['ID']; ?>">
                                <div class="h3"><?= htmlspecialcharsex($arProp["NAME"]); ?></div>
                                <ul class="<?= $arProp['CODE'] ?>">
                                    <? $props = array();
                                    if ($key !== "TSVET_TP") {
                                        foreach ($arProp["DISPLAY_VALUE"] as $i => $arOneValue) {
                                            $props[$key] = array(
                                                "NAME" => $arProp["NAME"],
                                                "CODE" => $arProp["CODE"],
                                                "VALUE" => strip_tags($arOneValue)
                                            );
//                                            ?>
                                            <!--                                            <pre>--><?//
//                                            print_r($arProp); ?><!--</pre>--><?//
                                            $props[$key] =
                                                strtr(base64_encode(addslashes(gzcompress(serialize($props[$key]), 9))),
                                                    '+/=', '-_,'); ?>
                                            <li data-select-onevalue="<?= $props[$key] ?>">
                                                <span title="<?= $arOneValue; ?>"><?= $arOneValue ?></span>
                                            </li>
                                            <?
                                        }
                                    } else {
                                        foreach ($arProp['PROPERTY_COLOR_PIC']["PICT_URL"] as $i => $arOneValue) { ?>
                                            <li>
                                                <a href="<?= $arResult["SELECT_PROPS"]['TSVET_TP']["URL"][$i] ?>">
                                                    <img src="<?= $arProp['PROPERTY_COLOR_PIC']["PICT_URL"][$i]['SRC'] ?>"
                                                         alt="">
                                                </a>
                                            </li>
                                        <? } ?>
                                    <? } ?>
                                </ul>
                            </div>
                        <?
//                        endif;
                        endforeach;
                        unset($arProp); ?>
                    </div>
                <? endif; ?>

                <!--                /***PROPERTIES***/-->

                <div class="catalog-detail-properties">
                    <div class="h4"><?= GetMessage("CATALOG_ELEMENT_PROPERTIES") ?></div>
                    <? if (isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])): ?>
                        <ul class="offer-props-list">

                            <? foreach ($arResult["OFFERS"] as $key => $arOffer):
                                $offerProps = $arOffer["DISPLAY_PROPERTIES"];
                                foreach ($offerProps as $prop => $value):
                                    if ($prop !== "OFFER_MORE_PHOTO"): ?>
                                        <li class="catalog-detail-property offer_prop offer_prop_<?= $arOffer['ID'] ?> <?= $arResult['ID'] ?> hidden">
                                            <span class="name"><?= $value["NAME"] ?></span>
                                            <span class="val"><?= $value["DISPLAY_VALUE"] ?></span>
                                        </li>
                                    <? endif; ?>
                                <?endforeach;
                            endforeach;

                            foreach ($arResult["DISPLAY_PROPERTIES"] as $k => $v):
                                if ($k !== "ARTNUMBER" && $k !== "VES_KG") {
                                    ?>
                                    <li class="catalog-detail-property">
                                        <span class="name"><?= $v["NAME"] ?></span>
                                        <span class="val"><?= is_array($v["DISPLAY_VALUE"]) ?
                                                implode(", ", $v["DISPLAY_VALUE"]) : $v["DISPLAY_VALUE"]; ?></span>
                                    </li>
                                    <?
                                }
//                                 if($k === "TSVET_TP") {
//
                                ?><!--<pre>--><? //
//
//                                print_r($v);
//                                }
//
                                ?><!--</pre>--><? //
                            endforeach; ?>
                        </ul>
                    <? else :
                        if (!empty($arResult["DISPLAY_PROPERTIES"])):
                            foreach ($arResult["DISPLAY_PROPERTIES"] as $k => $v): ?>
                                <div class="catalog-detail-property">
                                    <span class="name"><?= $v["NAME"] ?></span>
                                    <span class="val"><?= is_array($v["DISPLAY_VALUE"]) ?
                                            implode(", ", $v["DISPLAY_VALUE"]) : $v["DISPLAY_VALUE"]; ?></span>
                                </div>
                            <? endforeach;
                        endif;
                    endif; ?>
                </div>
            </div>
        </div>


        <? /***OFFERS_LIST***/
        if (isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])):
            if ($arSetting["OFFERS_VIEW"]["VALUE"] == "LIST"):?>
                <div id="catalog-detail-offers-list" class="catalog-detail-offers-list">
                    <div class="h3"><?= GetMessage("CATALOG_ELEMENT_OFFERS_LIST") ?></div>
                    <div class="offers-items">
                        <div class="thead">
                            <div class="offers-items-image"><?= GetMessage("CATALOG_ELEMENT_OFFERS_LIST_IMAGE") ?></div>
                            <div class="offers-items-name"><?= GetMessage("CATALOG_ELEMENT_OFFERS_LIST_NAME") ?></div>
                            <? $i = 1;
                            foreach ($arResult["SKU_PROPS"] as $arProp):
                                if (!isset($arResult["OFFERS_PROP"][$arProp["CODE"]]))
                                    continue;
                                if ($i > 3)
                                    continue; ?>
                                <div class="offers-items-prop"><?= htmlspecialcharsex($arProp["NAME"]); ?></div>
                                <? $i++;
                            endforeach; ?>
                            <div class="offers-items-price"></div>
                            <div class="offers-items-buy"><?= GetMessage("CATALOG_ELEMENT_OFFERS_LIST_PRICE") ?></div>
                        </div>
                        <div class="tbody">
                            <? foreach ($arResult["OFFERS"] as $keyOffer => $arOffer):
                                $sticker = "";
                                if ($arOffer["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"] > 0) {
                                    $sticker .= "<span class='discount'>-" .
                                        $arOffer["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"] . "%</span>";
                                } ?>
                                <div class="catalog-item" id="catalog-offer-item-<?= $arOffer['ID'] ?>">
                                    <div class="catalog-item-info">
                                        <div class="catalog-item-image-cont">
                                            <div class="catalog-item-image">
                                                <? if (!empty($arOffer["PREVIEW_IMG"]["SRC"])): ?>
                                                    <img src="<?= $arOffer['PREVIEW_IMG']['SRC'] ?>"
                                                         width="<?= $arOffer['PREVIEW_IMG']['WIDTH'] ?>"
                                                         height="<?= $arOffer['PREVIEW_IMG']['HEIGHT'] ?>"
                                                         alt="<?= (isset($arOffer['NAME']) &&
                                                             !empty($arOffer['NAME'])) ? $arOffer['NAME'] :
                                                             $arResult['NAME']; ?>"/>
                                                <? else: ?>
                                                    <img src="<?= $arResult['PREVIEW_IMG']['SRC'] ?>"
                                                         width="<?= $arResult['PREVIEW_IMG']['WIDTH'] ?>"
                                                         height="<?= $arResult['PREVIEW_IMG']['HEIGHT'] ?>"
                                                         alt="<?= (isset($arOffer['NAME']) &&
                                                             !empty($arOffer['NAME'])) ? $arOffer['NAME'] :
                                                             $arResult['NAME']; ?>"/>
                                                <? endif; ?>
                                                <div class="sticker">
                                                    <?= $sticker ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="catalog-item-title">
                                            <span class="name"><?= (isset($arOffer["NAME"]) &&
                                                    !empty($arOffer["NAME"])) ? $arOffer["NAME"] :
                                                    $arResult["NAME"]; ?></span>
                                            <div class="article">
                                                <?= GetMessage("CATALOG_ELEMENT_ARTNUMBER") ?>

                                                <? if (isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])): ?>
                                                    <ul class="offer-article-list">
                                                        <? foreach ($arResult["OFFERS"] as $key => $arOffer):
                                                            $offerArticle =
                                                                $arOffer["DISPLAY_PROPERTIES"]["ARTNUMBER"]["VALUE"];
                                                            if (!empty($offerArticle)):?>
                                                                <li class="offer_article offer_article_<?= $arOffer['ID'] ?> <?= $arResult['ID'] ?> hidden">
                                                                    <?= $offerArticle ?>
                                                                </li>
                                                            <? else : ?>
                                                                <li class="offer_article offer_article_<?= $arOffer['ID'] ?> <?= $arResult['ID'] ?> hidden">
                                                                    -
                                                                </li>
                                                            <?endif;
                                                        endforeach; ?>
                                                    </ul>

                                                <? else : ?>
                                                    <?= !empty($arResult["PROPERTIES"]["ARTNUMBER"]["VALUE"]) ?
                                                        $arResult["PROPERTIES"]["ARTNUMBER"]["VALUE"] : "-"; ?>
                                                <? endif; ?>

                                            </div>
                                        </div>
                                        <? if (!empty($arOffer["DISPLAY_PROPERTIES"])):
                                            $i = 1;
                                            foreach ($arOffer["DISPLAY_PROPERTIES"] as $k => $v):
                                                if (!isset($arResult["OFFERS_PROP"][$v["CODE"]]))
                                                    continue;
                                                if ($i > 3)
                                                    continue; ?>
                                                <div class="catalog-item-prop">
                                                    <? foreach ($arResult["SKU_PROPS"] as $arProp):
                                                        if ($arProp["CODE"] == $v["CODE"]):
                                                            if ($arProp["SHOW_MODE"] == "TEXT"):
                                                                echo strip_tags($v["DISPLAY_VALUE"]);
                                                            elseif ($arProp["SHOW_MODE"] == "PICT"):?>
                                                                <span class="prop_cont">
																<span class="prop"
                                                                      title="<?= $arProp['VALUES'][$v['VALUE']]['NAME'] ?>">
																	<? if (!empty($arProp["VALUES"][$v["VALUE"]]["PICT"]["src"])): ?>
                                                                        <img src="<?= $arProp['VALUES'][$v['VALUE']]['PICT']['src'] ?>"
                                                                             width="<?= $arProp['VALUES'][$v['VALUE']]['PICT']['width'] ?>"
                                                                             height="<?= $arProp['VALUES'][$v['VALUE']]['PICT']['height'] ?>"
                                                                             alt="<?= $arProp['VALUES'][$v['VALUE']]['NAME'] ?>"/>
                                                                    <? else: ?>
                                                                        <i style="background:#<?= $arProp['VALUES'][$v['VALUE']]['HEX'] ?>"></i>
                                                                    <? endif; ?>
																</span>
															</span>
                                                            <?endif;
                                                        endif;
                                                    endforeach; ?>
                                                </div>
                                                <? $i++;
                                            endforeach;
                                        endif; ?>
                                        <div class="item-price">
                                            <? foreach ($arOffer["PRICES"] as $code => $arPrice):
                                                if ($arPrice["MIN_PRICE"] == "Y"):
                                                    if ($arPrice["CAN_ACCESS"]):

                                                        $price = CCurrencyLang::GetCurrencyFormat($arPrice["CURRENCY"],
                                                            "ru");
                                                        if (empty($price["THOUSANDS_SEP"])):
                                                            $price["THOUSANDS_SEP"] = " ";
                                                        endif;
                                                        if ($price["HIDE_ZERO"] == "Y"):
                                                            if (
                                                                round($arPrice["DISCOUNT_VALUE"], $price["DECIMALS"]) ==
                                                                round($arPrice["DISCOUNT_VALUE"], 0)):
                                                                $price["DECIMALS"] = 0;
                                                            endif;
                                                        endif;
                                                        $currency = str_replace("#", " ", $price["FORMAT_STRING"]);

                                                        if ($arPrice["DISCOUNT_VALUE"] <= 0):
                                                            $arOffer["ASK_PRICE"] = 1; ?>
                                                            <span class="catalog-item-no-price">
															<span class="unit">
																<?= GetMessage("CATALOG_ELEMENT_NO_PRICE") ?>
                                                                <br/>
																<span><?= (!empty($arOffer["CATALOG_MEASURE_NAME"])) ?
                                                                        GetMessage("CATALOG_ELEMENT_UNIT") . " " .
                                                                        $arOffer["CATALOG_MEASURE_NAME"] : ""; ?></span>
															</span>
														</span>
                                                        <? else: ?>
                                                            <span class="catalog-item-price">
															<?= number_format($arPrice["DISCOUNT_VALUE"],
                                                                $price["DECIMALS"], $price["DEC_POINT"],
                                                                $price["THOUSANDS_SEP"]); ?>
                                                                <span class="unit">
																<?= $currency ?>
                                                                    <span><?= (!empty($arOffer["CATALOG_MEASURE_NAME"])) ?
                                                                            GetMessage("CATALOG_ELEMENT_UNIT") . " " .
                                                                            $arOffer["CATALOG_MEASURE_NAME"] : ""; ?></span>
															</span>
														</span>
                                                            <? if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]): ?>
                                                                <span class="catalog-item-price-old">
																<?= $arPrice["PRINT_VALUE"]; ?>
															</span>
                                                                <span class="catalog-item-price-percent">
																<?= GetMessage('CATALOG_ELEMENT_SKIDKA') ?>
                                                                    <br/>
                                                                    <?= $arPrice["PRINT_DISCOUNT_DIFF"] ?>
															</span>
                                                            <?endif;
                                                        endif;
                                                    endif;
                                                endif;
                                            endforeach; ?>
                                        </div>
                                        <? if (!empty($arOffer["DISPLAY_PROPERTIES"])): ?>
                                            <div class="catalog-item-props-mob"></div>
                                        <? endif; ?>
                                        <div class="buy_more">
                                            <div class="available">
                                                <? if ($arOffer["CAN_BUY"]): ?>
                                                    <div class="avl">
                                                        <i class="fa fa-check-circle"></i>
                                                        <span><?= GetMessage("CATALOG_ELEMENT_AVAILABLE") ?><?= in_array("PRODUCT_QUANTITY",
                                                                $arSetting["GENERAL_SETTINGS"]["VALUE"]) ?
                                                                " " . $arOffer["CATALOG_QUANTITY"] : "" ?></span>
                                                    </div>
                                                <? elseif (!$arOffer["CAN_BUY"]): ?>
                                                    <div class="not_avl">
                                                        <i class="fa fa-times-circle"></i>
                                                        <span><?= GetMessage("CATALOG_ELEMENT_NOT_AVAILABLE") ?></span>
                                                    </div>
                                                <? endif; ?>
                                            </div>
                                            <div class="clr"></div>
                                            <? if ($arOffer["CAN_BUY"]):
                                                if ($arOffer["ASK_PRICE"]):?>
                                                    <a class="btn_buy apuo" id="ask_price_anch_<?= $arOffer['ID'] ?>"
                                                       href="javascript:void(0)" rel="nofollow"><i
                                                                class="fa fa-comment-o"></i><span
                                                                class="short"><?= GetMessage("CATALOG_ELEMENT_ASK_PRICE_SHORT") ?></span></a>
                                                    <? $properties = false;
                                                    foreach ($arOffer["DISPLAY_PROPERTIES"] as $propOffer) {
                                                        $properties[] = $propOffer["NAME"] . ": " .
                                                            strip_tags($propOffer["DISPLAY_VALUE"]);
                                                    }
                                                    $properties = implode("; ", $properties);
                                                    if (!empty($properties)):
                                                        $offer_name =
                                                            ((isset($arOffer["NAME"]) && !empty($arOffer["NAME"])) ?
                                                                $arOffer["NAME"] : $arResult["NAME"]) . " (" .
                                                            $properties . ")";
                                                    else:
                                                        $offer_name =
                                                            (isset($arOffer["NAME"]) && !empty($arOffer["NAME"])) ?
                                                                $arOffer["NAME"] : $arResult["NAME"];
                                                    endif; ?>
                                                    <? $APPLICATION->IncludeComponent("altop:ask.price", "",
                                                        Array(
                                                            "ELEMENT_ID" => $arOffer["ID"],
                                                            "ELEMENT_NAME" => $offer_name,
                                                            "EMAIL_TO" => "",
                                                            "REQUIRED_FIELDS" => array("NAME", "TEL", "TIME")
                                                        ),
                                                        false,
                                                        array("HIDE_ICONS" => "Y")
                                                    ); ?>
                                                <? elseif (!$arOffer["ASK_PRICE"]): ?>
                                                    <div class="add2basket_block">
                                                        <? foreach ($arOffer["PRICES"] as $code => $arPrice):
                                                            if ($arPrice["MIN_PRICE"] == "Y"):
                                                                $props = array();
                                                                foreach ($arOffer["DISPLAY_PROPERTIES"] as $propOffer) {
                                                                    $props[] = array(
                                                                        "NAME" => $propOffer["NAME"],
                                                                        "CODE" => $propOffer["CODE"],
                                                                        "VALUE" => strip_tags($propOffer["DISPLAY_VALUE"])
                                                                    );
                                                                }
                                                                $props =
                                                                    strtr(base64_encode(addslashes(gzcompress(serialize($props),
                                                                        9))), '+/=', '-_,'); ?>
                                                                <div class="delay">
                                                                    <a href="javascript:void(0)"
                                                                       id="catalog-item-delay-<?= $arOffer['ID'] ?>"
                                                                       class="catalog-item-delay"
                                                                       onclick="return addToDelay('<?= $arOffer["ID"] ?>', '<?= $arOffer["CATALOG_MEASURE_RATIO"] ?>', '<?= $props ?>', '', 'catalog-item-delay-<?= $arOffer["ID"] ?>')"
                                                                       rel="nofollow"><i class="fa fa-heart-o"></i><i
                                                                                class="fa fa-check"></i></a>
                                                                </div>
                                                            <?endif;
                                                        endforeach; ?>
                                                        <form action="<?= SITE_DIR ?>ajax/add2basket.php"
                                                              id="add2basket_form_<?= $arOffer['ID'] ?>"
                                                              class="add2basket_form">
                                                            <div class="qnt_cont">
                                                                <a href="javascript:void(0)" class="minus"
                                                                   onclick="if (BX('quantity_<?= $arOffer["ID"] ?>').value > <?= $arOffer["CATALOG_MEASURE_RATIO"] ?>) BX('quantity_<?= $arOffer["ID"] ?>').value = parseFloat(BX('quantity_<?= $arOffer["ID"] ?>').value)-<?= $arOffer["CATALOG_MEASURE_RATIO"] ?>;"><span>-</span></a>
                                                                <input type="text" id="quantity_<?= $arOffer['ID'] ?>"
                                                                       name="quantity" class="quantity"
                                                                       value="<?= $arOffer['CATALOG_MEASURE_RATIO'] ?>"/>
                                                                <a href="javascript:void(0)" class="plus"
                                                                   onclick="BX('quantity_<?= $arOffer["ID"] ?>').value = parseFloat(BX('quantity_<?= $arOffer["ID"] ?>').value)+<?= $arOffer["CATALOG_MEASURE_RATIO"] ?>;"><span>+</span></a>
                                                            </div>
                                                            <input type="hidden" name="ID" class="offer_id"
                                                                   value="<?= $arOffer['ID'] ?>"/>
                                                            <? $props = array();
                                                            foreach ($arOffer["DISPLAY_PROPERTIES"] as $propOffer) {
                                                                $props[] = array(
                                                                    "NAME" => $propOffer["NAME"],
                                                                    "CODE" => $propOffer["CODE"],
                                                                    "VALUE" => strip_tags($propOffer["DISPLAY_VALUE"])
                                                                );
                                                            }
                                                            $props =
                                                                strtr(base64_encode(addslashes(gzcompress(serialize($props),
                                                                    9))), '+/=', '-_,'); ?>
                                                            <input type="hidden" name="PROPS" value="<?= $props ?>"/>
                                                            <? if (!empty($arResult["SELECT_PROPS"])): ?>
                                                                <input type="hidden" name="SELECT_PROPS"
                                                                       id="select_props_<?= $arOffer['ID'] ?>"
                                                                       value=""/>
                                                            <? endif; ?>
                                                            <? if (!empty($arOffer["PREVIEW_IMG"]["SRC"])): ?>
                                                                <input type="hidden" name="item_image"
                                                                       class="item_image"
                                                                       value="&lt;img class='item_image' src='<?= $arOffer["PREVIEW_IMG"]["SRC"] ?>' alt='<?= (isset($arOffer["NAME"]) &&
                                                                           !empty($arOffer["NAME"])) ?
                                                                           $arOffer["NAME"] :
                                                                           $arResult["NAME"]; ?>'/&gt;"/>
                                                            <? else: ?>
                                                                <input type="hidden" name="item_image"
                                                                       class="item_image"
                                                                       value="&lt;img class='item_image' src='<?= $arResult["PREVIEW_IMG"]["SRC"] ?>' alt='<?= (isset($arOffer["NAME"]) &&
                                                                           !empty($arOffer["NAME"])) ?
                                                                           $arOffer["NAME"] :
                                                                           $arResult["NAME"]; ?>'/&gt;"/>
                                                            <? endif; ?>
                                                            <input type="hidden" name="item_title" class="item_title"
                                                                   value="<?= (isset($arOffer['NAME']) &&
                                                                       !empty($arOffer['NAME'])) ? $arOffer['NAME'] :
                                                                       $arResult['NAME']; ?>"/>
                                                            <button type="submit" name="add2basket" class="btn_buy"
                                                                    value="<?= GetMessage('CATALOG_ELEMENT_ADD_TO_CART') ?>">
                                                                <i class="fa fa-shopping-cart"></i></button>
                                                            <small class="result offer-item hidden"><i
                                                                        class="fa fa-check"></i></small>
                                                        </form>
                                                        <button name="boc_anch" id="boc_anch_<?= $arOffer['ID'] ?>"
                                                                class="btn_buy boc_anch"
                                                                value="<?= GetMessage('CATALOG_ELEMENT_BOC') ?>"><i
                                                                    class="fa fa-bolt"></i><?= GetMessage('CATALOG_ELEMENT_BOC_SHORT') ?>
                                                        </button>
                                                        <? $APPLICATION->IncludeComponent("altop:buy.one.click",
                                                            ".default",
                                                            array(
                                                                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                                                                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                                                                "ELEMENT_ID" => $arOffer["ID"],
                                                                "ELEMENT_CODE" => "",
                                                                "ELEMENT_PROPS" => $props,
                                                                "REQUIRED_ORDER_FIELDS" => array(
                                                                    0 => "NAME",
                                                                    1 => "TEL",
                                                                ),
                                                                "DEFAULT_PERSON_TYPE" => "1",
                                                                "DEFAULT_ORDER_PROP_NAME" => "1",
                                                                "DEFAULT_ORDER_PROP_TEL" => "3",
                                                                "DEFAULT_ORDER_PROP_EMAIL" => "2",
                                                                "DEFAULT_DELIVERY" => "0",
                                                                "DEFAULT_PAYMENT" => "0",
                                                                "BUY_MODE" => "ONE",
                                                                "DUPLICATE_LETTER_TO_EMAILS" => array(
                                                                    0 => "admin",
                                                                ),
                                                            ),
                                                            false,
                                                            array("HIDE_ICONS" => "Y")
                                                        ); ?>
                                                    </div>
                                                <?endif;
                                            elseif (!$arOffer["CAN_BUY"]):?>
                                                <a class="btn_buy apuo" id="order_anch_<?= $arOffer['ID'] ?>"
                                                   href="javascript:void(0)" rel="nofollow"><i
                                                            class="fa fa-clock-o"></i><span
                                                            class="short"><?= GetMessage("CATALOG_ELEMENT_UNDER_ORDER_SHORT") ?></span></a>
                                                <? $properties = false;
                                                foreach ($arOffer["DISPLAY_PROPERTIES"] as $propOffer) {
                                                    $properties[] = $propOffer["NAME"] . ": " .
                                                        strip_tags($propOffer["DISPLAY_VALUE"]);
                                                }
                                                $properties = implode("; ", $properties);
                                                if (!empty($properties)):
                                                    $offer_name =
                                                        ((isset($arOffer["NAME"]) && !empty($arOffer["NAME"])) ?
                                                            $arOffer["NAME"] : $arResult["NAME"]) . " (" . $properties .
                                                        ")";
                                                else:
                                                    $offer_name =
                                                        (isset($arOffer["NAME"]) && !empty($arOffer["NAME"])) ?
                                                            $arOffer["NAME"] : $arResult["NAME"];
                                                endif; ?>
                                                <? $APPLICATION->IncludeComponent("altop:ask.price", "order",
                                                    Array(
                                                        "ELEMENT_ID" => $arOffer["ID"],
                                                        "ELEMENT_NAME" => $offer_name,
                                                        "EMAIL_TO" => "",
                                                        "REQUIRED_FIELDS" => array("NAME", "TEL", "TIME")
                                                    ),
                                                    false,
                                                    array("HIDE_ICONS" => "Y")
                                                ); ?>
                                            <? endif; ?>
                                        </div>
                                    </div>
                                </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                </div>
            <?endif;
        endif;

        /***KIT_ITEMS***/
        if (count($arResult["KIT_ITEMS"]) > 0):?>
            <div class="kit-items">
                <div class="h3"><?= GetMessage("CATALOG_ELEMENT_KIT_ITEMS") ?></div>
                <div class="catalog-item-cards">
                    <? foreach ($arResult["KIT_ITEMS"] as $key => $arItem): ?>
                        <div class="catalog-item-card">
                            <div class="catalog-item-info">
                                <div class="item-image-cont">
                                    <div class="item-image">
                                        <? if (is_array($arItem["PREVIEW_IMG"])): ?>
                                            <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                                                <img class="item_img" src="<?= $arItem['PREVIEW_IMG']['SRC'] ?>"
                                                     width="<?= $arItem['PREVIEW_IMG']['WIDTH'] ?>"
                                                     height="<?= $arItem['PREVIEW_IMG']['HEIGHT'] ?>"
                                                     alt="<?= $arItem['NAME'] ?>"/>
                                            </a>
                                        <? else: ?>
                                            <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                                                <img class="item_img"
                                                     src="<?= SITE_TEMPLATE_PATH ?>/images/no-photo.jpg" width="150"
                                                     height="150" alt="<?= $arItem['NAME'] ?>"/>
                                            </a>
                                        <? endif ?>
                                    </div>
                                </div>
                                <div class="item-all-title">
                                    <a class="item-title" href="<?= $arItem['DETAIL_PAGE_URL'] ?>"
                                       title="<?= $arItem['NAME'] ?>">
                                        <?= $arItem["NAME"] ?>
                                    </a>
                                </div>
                                <div class="item-price-cont">
                                    <? $price = CCurrencyLang::GetCurrencyFormat($arItem["PRICE_CURRENCY"], "ru");
                                    if (empty($price["THOUSANDS_SEP"])):
                                        $price["THOUSANDS_SEP"] = " ";
                                    endif;
                                    if ($price["HIDE_ZERO"] == "Y"):
                                        if (
                                            round($arItem["PRICE_DISCOUNT_VALUE"], $price["DECIMALS"]) ==
                                            round($arItem["PRICE_DISCOUNT_VALUE"], 0)):
                                            $price["DECIMALS"] = 0;
                                        endif;
                                    endif;
                                    $currency = str_replace("#", " ", $price["FORMAT_STRING"]); ?>

                                    <div class="item-price">
                                        <? if ($arItem["PRICE_DISCOUNT_VALUE"] < $arItem["PRICE_VALUE"]): ?>
                                            <span class="catalog-item-price-old">
											<?= $arItem["PRICE_PRINT_VALUE"]; ?>
										</span>
                                        <? endif; ?>
                                        <span class="catalog-item-price">
										<?= number_format($arItem["PRICE_DISCOUNT_VALUE"], $price["DECIMALS"],
                                            $price["DEC_POINT"], $price["THOUSANDS_SEP"]); ?>
                                            <span class="unit"><?= $currency ?></span>
									</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? endforeach; ?>
                </div>
                <div class="clr"></div>
            </div>
        <?endif;

        /***SET_CONSTRUCTOR***/ ?>
        <div id="set-constructor-items-to"></div>

        <? /***TABS***/ ?>
        <div class="section">
            <ul class="tabs">
                <li class="current">
                    <a href="#tab1"><span><?= GetMessage("CATALOG_ELEMENT_FULL_DESCRIPTION") ?></span></a>
                </li>
                <li<?= (empty($arResult["PROPERTIES"]["FREE_TAB"]["VALUE"])) ? " style='display:none;'" : ""; ?>>
                    <a href="#tab2"><span><?= $arResult["PROPERTIES"]["FREE_TAB"]["NAME"] ?></span></a>
                </li>
                <li<?= (empty($arResult["PROPERTIES"]["ACCESSORIES"]["VALUE"])) ? " style='display:none;'" : ""; ?>>
                    <a href="#tab3"><span><?= $arResult["PROPERTIES"]["ACCESSORIES"]["NAME"] ?></span></a>
                </li>
                <li>
                    <a href="#tab4"><span><?= GetMessage("CATALOG_ELEMENT_REVIEWS") ?> <span
                                    class="reviews_count">(<?= $arResult["REVIEWS"]["COUNT"] ?>)</span></span></a>
                </li>
                <li<?= (isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])) ?
                    (($arSetting["OFFERS_VIEW"]["VALUE"] == "LIST") ? " style='display:none;'" : "") : ""; ?>>
                    <a href="#tab5"><span><?= GetMessage("CATALOG_ELEMENT_SHOPS") ?></span></a>
                </li>
            </ul>
            <div class="box visible">
                <div class="description">
                    <?= $arResult["DETAIL_TEXT"]; ?>
                </div>
            </div>
            <div class="box"<?= (empty($arResult["PROPERTIES"]["FREE_TAB"]["VALUE"])) ? " style='display:none;'" :
                ""; ?>>
                <div class="tab-content">
                    <?= $arResult["PROPERTIES"]["FREE_TAB"]["~VALUE"]["TEXT"]; ?>
                </div>
            </div>
            <div class="box"
                 id="accessories-to"<?= (empty($arResult["PROPERTIES"]["ACCESSORIES"]["VALUE"])) ?
                " style='display:none;'" : ""; ?>></div>
            <div class="box" id="catalog-reviews-to"></div>
            <div class="box"<?= (isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])) ?
                (($arSetting["OFFERS_VIEW"]["VALUE"] == "LIST") ? " style='display:none;'" : "") : ""; ?>>
                <div id="<?= $arItemIDs['STORE']; ?>">
                    <? /***STORES***/
                    if (isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])):
                        if ($arSetting["OFFERS_VIEW"]["VALUE"] != "LIST"):
                            foreach ($arResult["OFFERS"] as $key => $arOffer):?>
                                <div id="catalog-detail-stores-<?= $arOffer['ID'] ?>"
                                     class="catalog-detail-stores <?= $arResult['ID'] ?> hidden">
                                    <? $APPLICATION->IncludeComponent("bitrix:catalog.store.amount", ".default",
                                        array(
                                            "ELEMENT_ID" => $arOffer["ID"],
                                            "STORE_PATH" => $arParams["STORE_PATH"],
                                            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                                            "CACHE_TIME" => $arParams["CACHE_TIME"],
                                            "MAIN_TITLE" => $arParams["MAIN_TITLE"],
                                            "USE_STORE_PHONE" => $arParams["USE_STORE_PHONE"],
                                            "SCHEDULE" => $arParams["USE_STORE_SCHEDULE"],
                                            "USE_MIN_AMOUNT" => $arParams["USE_MIN_AMOUNT"],
                                            "MIN_AMOUNT" => $arParams["MIN_AMOUNT"],
                                            "STORES" => $arParams['STORES'],
                                            "SHOW_EMPTY_STORE" => $arParams['SHOW_EMPTY_STORE'],
                                            "SHOW_GENERAL_STORE_INFORMATION" => $arParams['SHOW_GENERAL_STORE_INFORMATION'],
                                            "USER_FIELDS" => $arParams['USER_FIELDS'],
                                            "FIELDS" => $arParams['FIELDS']
                                        ),
                                        false,
                                        array("HIDE_ICONS" => "Y")
                                    ); ?>
                                </div>
                            <?endforeach;
                        endif;
                    else:?>
                        <div class="catalog-detail-stores">
                            <? $APPLICATION->IncludeComponent("bitrix:catalog.store.amount", ".default",
                                array(
                                    "ELEMENT_ID" => $arResult["ID"],
                                    "STORE_PATH" => $arParams["STORE_PATH"],
                                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                                    "MAIN_TITLE" => $arParams["MAIN_TITLE"],
                                    "USE_STORE_PHONE" => $arParams["USE_STORE_PHONE"],
                                    "SCHEDULE" => $arParams["USE_STORE_SCHEDULE"],
                                    "USE_MIN_AMOUNT" => $arParams["USE_MIN_AMOUNT"],
                                    "MIN_AMOUNT" => $arParams["MIN_AMOUNT"],
                                    "STORES" => $arParams['STORES'],
                                    "SHOW_EMPTY_STORE" => $arParams['SHOW_EMPTY_STORE'],
                                    "SHOW_GENERAL_STORE_INFORMATION" => $arParams['SHOW_GENERAL_STORE_INFORMATION'],
                                    "USER_FIELDS" => $arParams['USER_FIELDS'],
                                    "FIELDS" => $arParams['FIELDS']
                                ),
                                false,
                                array("HIDE_ICONS" => "Y")
                            ); ?>
                        </div>
                    <? endif; ?>
                </div>
            </div>
        </div>
        <div class="clr"></div>
    </div>

    <? if (isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])) {
        $arJSParams = array(
            "CONFIG" => array(
                "USE_CATALOG" => $arResult["CATALOG"],
            ),
            "PRODUCT_TYPE" => $arResult["CATALOG_TYPE"],
            "VISUAL" => array(
                "ID" => $arItemIDs["ID"],
                "PICT_ID" => $arItemIDs["PICT"],
                "PRICE_ID" => $arItemIDs["PRICE"],
                "BUY_ID" => $arItemIDs["BUY"],
                "DELAY_ID" => $arItemIDs["DELAY"],
                "STORE_ID" => $arItemIDs["STORE"],
                "TREE_ID" => $arItemIDs["PROP_DIV"],
                "TREE_ITEM_ID" => $arItemIDs["PROP"],
            ),
            "PRODUCT" => array(
                "ID" => $arResult["ID"],
                "NAME" => $arResult["~NAME"]
            ),
            "OFFERS_VIEW" => $arSetting["OFFERS_VIEW"]["VALUE"],
            "OFFERS" => $arResult["JS_OFFERS"],
            "OFFER_SELECTED" => $arResult["OFFERS_SELECTED"],
            "TREE_PROPS" => $arSkuProps
        );
    } else {
        $arJSParams = array(
            "CONFIG" => array(
                "USE_CATALOG" => $arResult["CATALOG"]
            ),
            "PRODUCT_TYPE" => $arResult["CATALOG_TYPE"],
            "VISUAL" => array(
                "ID" => $arItemIDs["ID"],
            ),
            "PRODUCT" => array(
                "ID" => $arResult["ID"],
                "NAME" => $arResult["~NAME"]
            )
        );
    }

    if (isset($arResult["SELECT_PROPS"]) && !empty($arResult["SELECT_PROPS"])) {
        $arJSParams["VISUAL"]["SELECT_PROP_ID"] = $arItemIDs["SELECT_PROP_DIV"];
        $arJSParams["VISUAL"]["SELECT_PROP_ITEM_ID"] = $arItemIDs["SELECT_PROP"];
        $arJSParams["SELECT_PROPS"] = $arSelProps;
    } ?>

    <script type="text/javascript">
        var <?=$strObName;?> =
        new JCCatalogElement(<?=CUtil::PhpToJSObject($arJSParams, false, true);?>);
        BX.message({
            SITE_ID: "<?=SITE_ID;?>"
        });
    </script>


    <div id="product-set_pop-up">
        <? foreach ($arResult["OFFERS"] as $key => $arOffer): ?>
            <? if ($arOffer["DISPLAY_PROPERTIES"]["ACCESSORIES_TP"]["VALUE"]): ?>
                <div class="accessories_set offer_accessories offer_accessories_<?= $arOffer['ID'] ?> <?= $arResult['ID'] ?> hidden">

                    <? $APPLICATION->IncludeComponent(
                        "bitrix:catalog.section",
                        "accessories",
                        Array(
                            "ACTION_VARIABLE" => "action",
                            "ADD_PICT_PROP" => "-",
                            "ADD_PROPERTIES_TO_BASKET" => "Y",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "ADD_TO_BASKET_ACTION" => "ADD",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "BACKGROUND_IMAGE" => "-",
                            "BASKET_URL" => "/personal/basket.php",
                            "BROWSER_TITLE" => "-",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "Y",
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "COMPATIBLE_MODE" => "Y",
                            "CONVERT_CURRENCY" => "N",
                            "CUSTOM_FILTER" => "",
                            "DETAIL_URL" => "",
                            "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "DISPLAY_COMPARE" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "ELEMENT_SORT_FIELD" => "property_QUANTITY",
                            "ELEMENT_SORT_FIELD2" => "id",
                            "ELEMENT_SORT_ORDER" => "desc",
                            "ELEMENT_SORT_ORDER2" => "desc",
                            "ENLARGE_PRODUCT" => "STRICT",
                            "FILTER_NAME" => "arrFilter",
                            "HIDE_NOT_AVAILABLE" => "N",
                            "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                            "IBLOCK_ID" => "15",
                            "IBLOCK_TYPE" => "catalog",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "LABEL_PROP" => array(),
                            "LAZY_LOAD" => "N",
                            "LINE_ELEMENT_COUNT" => "3",
                            "LOAD_ON_SCROLL" => "N",
                            "MESSAGE_404" => "",
                            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                            "MESS_BTN_BUY" => "Купить",
                            "MESS_BTN_DETAIL" => "Подробнее",
                            "MESS_BTN_SUBSCRIBE" => "Подписаться",
                            "MESS_NOT_AVAILABLE" => "Нет в наличии",
                            "META_DESCRIPTION" => "-",
                            "META_KEYWORDS" => "-",
                            "OFFERS_LIMIT" => "5",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => ".default",
                            "PAGER_TITLE" => "Товары",
                            "PAGE_ELEMENT_COUNT" => "18",
                            "PARTIAL_PRODUCT_PROPERTIES" => "N",
                            "PRICE_CODE" => array(
                                0 => "BASE",
                            ),
                            "PRICE_VAT_INCLUDE" => "Y",
                            "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
                            "PRODUCT_ID_VARIABLE" => "id",
                            "PRODUCT_PROPERTIES" => array(),
                            "PRODUCT_PROPS_VARIABLE" => "prop",
                            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                            "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
                            "PRODUCT_SUBSCRIPTION" => "Y",
                            "PROPERTY_CODE" => array("", ""),
                            "PROPERTY_CODE_MOBILE" => array(),
                            "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
                            "RCM_TYPE" => "personal",
                            "SECTION_CODE" => "",
                            "SECTION_ID" => $arOffer["DISPLAY_PROPERTIES"]["ACCESSORIES_TP"]["VALUE"],
                            "SECTION_ID_VARIABLE" => "SECTION_ID",
                            "SECTION_URL" => "",
                            "SECTION_USER_FIELDS" => array("", ""),
                            "SEF_MODE" => "N",
                            "SET_BROWSER_TITLE" => "Y",
                            "SET_LAST_MODIFIED" => "N",
                            "SET_META_DESCRIPTION" => "Y",
                            "SET_META_KEYWORDS" => "Y",
                            "SET_STATUS_404" => "N",
                            "SET_TITLE" => "Y",
                            "SHOW_404" => "N",
                            "SHOW_ALL_WO_SECTION" => "N",
                            "SHOW_CLOSE_POPUP" => "N",
                            "SHOW_DISCOUNT_PERCENT" => "N",
                            "SHOW_FROM_SECTION" => "N",
                            "SHOW_MAX_QUANTITY" => "N",
                            "SHOW_OLD_PRICE" => "N",
                            "SHOW_PRICE_COUNT" => "1",
                            "SHOW_SLIDER" => "Y",
                            "SLIDER_INTERVAL" => "3000",
                            "SLIDER_PROGRESS" => "N",
                            "TEMPLATE_THEME" => "blue",
                            "USE_ENHANCED_ECOMMERCE" => "N",
                            "USE_MAIN_ELEMENT_SECTION" => "N",
                            "USE_PRICE_COUNT" => "N",
                            "USE_PRODUCT_QUANTITY" => "N"
                        )
                    ); ?>
                </div>
            <? endif; ?>
        <? endforeach; ?>
    </div>
</div>
<div class="related-sections">
    <div class="h3"><?= GetMessage("CATALOG_FURNITURE") ?></div>
    <ul class="related-sections-list">
        <? if (!empty($arResult['PROPERTIES']['ZVONKI']['VALUE'])): ?>
            <?
            global $arrSectFilter;
            ?>
            <li class="related-sections-item rings">
                <img
                        class="related-section-image"
                        src="/upload/iblock/eae/eae147bff8593e55933a88f9b61a8f1f.jpg"
                        alt="Звонки">
                <span class="related-section-name"><?= GetMessage("CATALOG_RINGS") ?>
                </span>
                <? $arrSectFilter = Array("ID", "XML_ID" => $arResult['PROPERTIES']['ZVONKI']['VALUE']); ?>
                <div id="rings" class="section_popup">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:catalog.section",
                        "table",
                        Array(
                            "ACTION_VARIABLE" => "action",
                            "ADD_PICT_PROP" => "-",
                            "ADD_PROPERTIES_TO_BASKET" => "Y",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "ADD_TO_BASKET_ACTION" => "ADD",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "BACKGROUND_IMAGE" => "-",
                            "BASKET_URL" => "/personal/basket.php",
                            "BROWSER_TITLE" => "-",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "Y",
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "COMPATIBLE_MODE" => "Y",
                            "CONVERT_CURRENCY" => "N",
                            "CUSTOM_FILTER" => "",
                            "DETAIL_URL" => "",
                            "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "DISPLAY_COMPARE" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "ELEMENT_SORT_FIELD" => "sort",
                            "ELEMENT_SORT_FIELD2" => "id",
                            "ELEMENT_SORT_ORDER" => "asc",
                            "ELEMENT_SORT_ORDER2" => "desc",
                            "ENLARGE_PRODUCT" => "STRICT",
                            "FILTER_NAME" => "arrSectFilter",
                            "HIDE_NOT_AVAILABLE" => "N",
                            "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                            "IBLOCK_ID" => "3",
                            "IBLOCK_TYPE" => "catalog",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "LABEL_PROP" => array(),
                            "LAZY_LOAD" => "N",
                            "LINE_ELEMENT_COUNT" => "3",
                            "LOAD_ON_SCROLL" => "N",
                            "MESSAGE_404" => "",
                            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                            "MESS_BTN_BUY" => "Купить",
                            "MESS_BTN_DETAIL" => "Подробнее",
                            "MESS_BTN_SUBSCRIBE" => "Подписаться",
                            "MESS_NOT_AVAILABLE" => "Нет в наличии",
                            "META_DESCRIPTION" => "-",
                            "META_KEYWORDS" => "-",
                            "OFFERS_CART_PROPERTIES" => array(),
                            "OFFERS_FIELD_CODE" => array("", ""),
                            "OFFERS_LIMIT" => "5",
                            "OFFERS_PROPERTY_CODE" => array("", ""),
                            "OFFERS_SORT_FIELD" => "sort",
                            "OFFERS_SORT_FIELD2" => "id",
                            "OFFERS_SORT_ORDER" => "asc",
                            "OFFERS_SORT_ORDER2" => "desc",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => ".default",
                            "PAGER_TITLE" => "Товары",
                            "PAGE_ELEMENT_COUNT" => "3",
                            "PARTIAL_PRODUCT_PROPERTIES" => "N",
                            "PRICE_CODE" => array("BASE"),
                            "PRICE_VAT_INCLUDE" => "Y",
                            "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
                            "PRODUCT_DISPLAY_MODE" => "N",
                            "PRODUCT_ID_VARIABLE" => "id",
                            "PRODUCT_PROPERTIES" => array(),
                            "PRODUCT_PROPS_VARIABLE" => "prop",
                            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                            "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
                            "PRODUCT_SUBSCRIPTION" => "Y",
                            "PROPERTY_CODE" => array("", ""),
                            "PROPERTY_CODE_MOBILE" => array(),
                            "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
                            "RCM_TYPE" => "personal",
                            "SECTION_CODE" => "",
                            "SECTION_CODE_PATH" => "",
                            "SECTION_ID" => '',
                            "SECTION_ID_VARIABLE" => "SECTION_ID",
                            "SECTION_URL" => "",
                            "SECTION_USER_FIELDS" => array("", ""),
                            "SEF_MODE" => "Y",
                            "SEF_RULE" => "",
                            "SET_BROWSER_TITLE" => "Y",
                            "SET_LAST_MODIFIED" => "N",
                            "SET_META_DESCRIPTION" => "Y",
                            "SET_META_KEYWORDS" => "Y",
                            "SET_STATUS_404" => "N",
                            "SET_TITLE" => "Y",
                            "SHOW_404" => "N",
                            "SHOW_ALL_WO_SECTION" => "N",
                            "SHOW_CLOSE_POPUP" => "N",
                            "SHOW_DISCOUNT_PERCENT" => "N",
                            "SHOW_FROM_SECTION" => "N",
                            "SHOW_MAX_QUANTITY" => "N",
                            "SHOW_OLD_PRICE" => "N",
                            "SHOW_PRICE_COUNT" => "1",
                            "SHOW_SLIDER" => "N",
                            "SLIDER_INTERVAL" => "3000",
                            "SLIDER_PROGRESS" => "N",
                            "TEMPLATE_THEME" => "blue",
                            "USE_ENHANCED_ECOMMERCE" => "N",
                            "USE_MAIN_ELEMENT_SECTION" => "N",
                            "USE_PRICE_COUNT" => "N",
                            "USE_PRODUCT_QUANTITY" => "N"
                        )
                    ); ?>
                </div>
            </li>
        <? endif; ?>
        <? if (!empty($arResult['PROPERTIES']['LOCKS']['VALUE'])): ?>
            <? global $arrSectFilter1;
            ?>
            <li class="related-sections-item locks">
                <img
                        class="related-section-image"
                        src="/upload/iblock/be3/be3184098c24892cbc18aa6eeb234d69.jpg"
                        alt="<?= GetMessage("CATALOG_LOCKS") ?>">
                <span class="related-section-name"><?= GetMessage("CATALOG_LOCKS") ?></span>
                <? $arrSectFilter1 = Array("ID" => $arResult['PROPERTIES']['LOCKS']['VALUE']); ?>
                <div id="locks" class="section_popup">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:catalog.section",
                        "table",
                        Array(
                            "ACTION_VARIABLE" => "action",
                            "ADD_PICT_PROP" => "-",
                            "ADD_PROPERTIES_TO_BASKET" => "Y",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "ADD_TO_BASKET_ACTION" => "ADD",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "BACKGROUND_IMAGE" => "-",
                            "BASKET_URL" => "/personal/basket.php",
                            "BROWSER_TITLE" => "-",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "Y",
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "COMPATIBLE_MODE" => "Y",
                            "CONVERT_CURRENCY" => "N",
                            "CUSTOM_FILTER" => "",
                            "DETAIL_URL" => "",
                            "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "DISPLAY_COMPARE" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "ELEMENT_SORT_FIELD" => "sort",
                            "ELEMENT_SORT_FIELD2" => "id",
                            "ELEMENT_SORT_ORDER" => "asc",
                            "ELEMENT_SORT_ORDER2" => "desc",
                            "ENLARGE_PRODUCT" => "STRICT",
                            "FILTER_NAME" => "arrSectFilter1",
                            "HIDE_NOT_AVAILABLE" => "N",
                            "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                            "IBLOCK_ID" => "3",
                            "IBLOCK_TYPE" => "catalog",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "LABEL_PROP" => array(),
                            "LAZY_LOAD" => "N",
                            "LINE_ELEMENT_COUNT" => "3",
                            "LOAD_ON_SCROLL" => "N",
                            "MESSAGE_404" => "",
                            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                            "MESS_BTN_BUY" => "Купить",
                            "MESS_BTN_DETAIL" => "Подробнее",
                            "MESS_BTN_SUBSCRIBE" => "Подписаться",
                            "MESS_NOT_AVAILABLE" => "Нет в наличии",
                            "META_DESCRIPTION" => "-",
                            "META_KEYWORDS" => "-",
                            "OFFERS_CART_PROPERTIES" => array(),
                            "OFFERS_FIELD_CODE" => array("", ""),
                            "OFFERS_LIMIT" => "5",
                            "OFFERS_PROPERTY_CODE" => array("", ""),
                            "OFFERS_SORT_FIELD" => "sort",
                            "OFFERS_SORT_FIELD2" => "id",
                            "OFFERS_SORT_ORDER" => "asc",
                            "OFFERS_SORT_ORDER2" => "desc",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => ".default",
                            "PAGER_TITLE" => "Товары",
                            "PAGE_ELEMENT_COUNT" => "3",
                            "PARTIAL_PRODUCT_PROPERTIES" => "N",
                            "PRICE_CODE" => array("BASE"),
                            "PRICE_VAT_INCLUDE" => "Y",
                            "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
                            "PRODUCT_DISPLAY_MODE" => "N",
                            "PRODUCT_ID_VARIABLE" => "id",
                            "PRODUCT_PROPERTIES" => array(),
                            "PRODUCT_PROPS_VARIABLE" => "prop",
                            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                            "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
                            "PRODUCT_SUBSCRIPTION" => "Y",
                            "PROPERTY_CODE" => array("", ""),
                            "PROPERTY_CODE_MOBILE" => array(),
                            "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
                            "RCM_TYPE" => "personal",
                            "SECTION_CODE" => "",
                            "SECTION_CODE_PATH" => "",
                            "SECTION_ID" => '',
                            "SECTION_ID_VARIABLE" => "SECTION_ID",
                            "SECTION_URL" => "",
                            "SECTION_USER_FIELDS" => array("", ""),
                            "SEF_MODE" => "Y",
                            "SEF_RULE" => "",
                            "SET_BROWSER_TITLE" => "Y",
                            "SET_LAST_MODIFIED" => "N",
                            "SET_META_DESCRIPTION" => "Y",
                            "SET_META_KEYWORDS" => "Y",
                            "SET_STATUS_404" => "N",
                            "SET_TITLE" => "Y",
                            "SHOW_404" => "N",
                            "SHOW_ALL_WO_SECTION" => "N",
                            "SHOW_CLOSE_POPUP" => "N",
                            "SHOW_DISCOUNT_PERCENT" => "N",
                            "SHOW_FROM_SECTION" => "N",
                            "SHOW_MAX_QUANTITY" => "N",
                            "SHOW_OLD_PRICE" => "N",
                            "SHOW_PRICE_COUNT" => "1",
                            "SHOW_SLIDER" => "N",
                            "SLIDER_INTERVAL" => "3000",
                            "SLIDER_PROGRESS" => "N",
                            "TEMPLATE_THEME" => "blue",
                            "USE_ENHANCED_ECOMMERCE" => "N",
                            "USE_MAIN_ELEMENT_SECTION" => "N",
                            "USE_PRICE_COUNT" => "N",
                            "USE_PRODUCT_QUANTITY" => "N"
                        )
                    ); ?>
                </div>
            </li>
        <? endif; ?>
        <? if (!empty($arResult['PROPERTIES']['TSYLINDRY']['VALUE'])): ?>
            <? global $arrSectFilter2;
            ?>
            <li class="related-sections-item tsylindry">
                <img
                        class="related-section-image"
                        src="/upload/iblock/af7/af793b954dd623f2237adf44b471c62c.jpg"
                        alt="<?= GetMessage("CATALOG_LOCKS") ?>">
                <span class="related-section-name"><?= GetMessage("CATALOG_TSYLINDRY") ?></span>
                <? $arrSectFilter2 = Array("ID" => $arResult['PROPERTIES']['TSYLINDRY']['VALUE']); ?>
                <div id="tsylindry" class="section_popup">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:catalog.section",
                        "table",
                        Array(
                            "ACTION_VARIABLE" => "action",
                            "ADD_PICT_PROP" => "-",
                            "ADD_PROPERTIES_TO_BASKET" => "Y",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "ADD_TO_BASKET_ACTION" => "ADD",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "BACKGROUND_IMAGE" => "-",
                            "BASKET_URL" => "/personal/basket.php",
                            "BROWSER_TITLE" => "-",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "Y",
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "COMPATIBLE_MODE" => "Y",
                            "CONVERT_CURRENCY" => "N",
                            "CUSTOM_FILTER" => "",
                            "DETAIL_URL" => "",
                            "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "DISPLAY_COMPARE" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "ELEMENT_SORT_FIELD" => "sort",
                            "ELEMENT_SORT_FIELD2" => "id",
                            "ELEMENT_SORT_ORDER" => "asc",
                            "ELEMENT_SORT_ORDER2" => "desc",
                            "ENLARGE_PRODUCT" => "STRICT",
                            "FILTER_NAME" => "arrSectFilter2",
                            "HIDE_NOT_AVAILABLE" => "N",
                            "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                            "IBLOCK_ID" => "3",
                            "IBLOCK_TYPE" => "catalog",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "LABEL_PROP" => array(),
                            "LAZY_LOAD" => "N",
                            "LINE_ELEMENT_COUNT" => "3",
                            "LOAD_ON_SCROLL" => "N",
                            "MESSAGE_404" => "",
                            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                            "MESS_BTN_BUY" => "Купить",
                            "MESS_BTN_DETAIL" => "Подробнее",
                            "MESS_BTN_SUBSCRIBE" => "Подписаться",
                            "MESS_NOT_AVAILABLE" => "Нет в наличии",
                            "META_DESCRIPTION" => "-",
                            "META_KEYWORDS" => "-",
                            "OFFERS_CART_PROPERTIES" => array(),
                            "OFFERS_FIELD_CODE" => array("", ""),
                            "OFFERS_LIMIT" => "5",
                            "OFFERS_PROPERTY_CODE" => array("", ""),
                            "OFFERS_SORT_FIELD" => "sort",
                            "OFFERS_SORT_FIELD2" => "id",
                            "OFFERS_SORT_ORDER" => "asc",
                            "OFFERS_SORT_ORDER2" => "desc",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => ".default",
                            "PAGER_TITLE" => "Товары",
                            "PAGE_ELEMENT_COUNT" => "3",
                            "PARTIAL_PRODUCT_PROPERTIES" => "N",
                            "PRICE_CODE" => array("BASE"),
                            "PRICE_VAT_INCLUDE" => "Y",
                            "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
                            "PRODUCT_DISPLAY_MODE" => "N",
                            "PRODUCT_ID_VARIABLE" => "id",
                            "PRODUCT_PROPERTIES" => array(),
                            "PRODUCT_PROPS_VARIABLE" => "prop",
                            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                            "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
                            "PRODUCT_SUBSCRIPTION" => "Y",
                            "PROPERTY_CODE" => array("", ""),
                            "PROPERTY_CODE_MOBILE" => array(),
                            "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
                            "RCM_TYPE" => "personal",
                            "SECTION_CODE" => "",
                            "SECTION_CODE_PATH" => "",
                            "SECTION_ID" => '',
                            "SECTION_ID_VARIABLE" => "SECTION_ID",
                            "SECTION_URL" => "",
                            "SECTION_USER_FIELDS" => array("", ""),
                            "SEF_MODE" => "Y",
                            "SEF_RULE" => "",
                            "SET_BROWSER_TITLE" => "Y",
                            "SET_LAST_MODIFIED" => "N",
                            "SET_META_DESCRIPTION" => "Y",
                            "SET_META_KEYWORDS" => "Y",
                            "SET_STATUS_404" => "N",
                            "SET_TITLE" => "Y",
                            "SHOW_404" => "N",
                            "SHOW_ALL_WO_SECTION" => "N",
                            "SHOW_CLOSE_POPUP" => "N",
                            "SHOW_DISCOUNT_PERCENT" => "N",
                            "SHOW_FROM_SECTION" => "N",
                            "SHOW_MAX_QUANTITY" => "N",
                            "SHOW_OLD_PRICE" => "N",
                            "SHOW_PRICE_COUNT" => "1",
                            "SHOW_SLIDER" => "N",
                            "SLIDER_INTERVAL" => "3000",
                            "SLIDER_PROGRESS" => "N",
                            "TEMPLATE_THEME" => "blue",
                            "USE_ENHANCED_ECOMMERCE" => "N",
                            "USE_MAIN_ELEMENT_SECTION" => "N",
                            "USE_PRICE_COUNT" => "N",
                            "USE_PRODUCT_QUANTITY" => "N"
                        )
                    ); ?>
                </div>
            </li>
        <? endif; ?>
        <? if (!empty($arResult['PROPERTIES']['NAKLADKI']['VALUE'])): ?>
            <? global $arrSectFilter3;
            ?>
            <li class="related-sections-item nakladki">
                <img
                        class="related-section-image"
                        src="/upload/iblock/d46/d46097cc23113e6535426440d4ebade9.jpg"
                        alt="<?= GetMessage("CATALOG_NAKLADKI") ?>">
                <span class="related-section-name"><?= GetMessage("CATALOG_NAKLADKI") ?></span>
                <? $arrSectFilter3 = Array("ID" => $arResult['PROPERTIES']['NAKLADKI']['VALUE']); ?>
                <div id="nakladki" class="section_popup">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:catalog.section",
                        "table",
                        Array(
                            "ACTION_VARIABLE" => "action",
                            "ADD_PICT_PROP" => "-",
                            "ADD_PROPERTIES_TO_BASKET" => "Y",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "ADD_TO_BASKET_ACTION" => "ADD",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "BACKGROUND_IMAGE" => "-",
                            "BASKET_URL" => "/personal/basket.php",
                            "BROWSER_TITLE" => "-",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "Y",
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "COMPATIBLE_MODE" => "Y",
                            "CONVERT_CURRENCY" => "N",
                            "CUSTOM_FILTER" => "",
                            "DETAIL_URL" => "",
                            "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "DISPLAY_COMPARE" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "ELEMENT_SORT_FIELD" => "sort",
                            "ELEMENT_SORT_FIELD2" => "id",
                            "ELEMENT_SORT_ORDER" => "asc",
                            "ELEMENT_SORT_ORDER2" => "desc",
                            "ENLARGE_PRODUCT" => "STRICT",
                            "FILTER_NAME" => "arrSectFilter3",
                            "HIDE_NOT_AVAILABLE" => "N",
                            "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                            "IBLOCK_ID" => "3",
                            "IBLOCK_TYPE" => "catalog",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "LABEL_PROP" => array(),
                            "LAZY_LOAD" => "N",
                            "LINE_ELEMENT_COUNT" => "3",
                            "LOAD_ON_SCROLL" => "N",
                            "MESSAGE_404" => "",
                            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                            "MESS_BTN_BUY" => "Купить",
                            "MESS_BTN_DETAIL" => "Подробнее",
                            "MESS_BTN_SUBSCRIBE" => "Подписаться",
                            "MESS_NOT_AVAILABLE" => "Нет в наличии",
                            "META_DESCRIPTION" => "-",
                            "META_KEYWORDS" => "-",
                            "OFFERS_CART_PROPERTIES" => array(),
                            "OFFERS_FIELD_CODE" => array("", ""),
                            "OFFERS_LIMIT" => "5",
                            "OFFERS_PROPERTY_CODE" => array("", ""),
                            "OFFERS_SORT_FIELD" => "sort",
                            "OFFERS_SORT_FIELD2" => "id",
                            "OFFERS_SORT_ORDER" => "asc",
                            "OFFERS_SORT_ORDER2" => "desc",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => ".default",
                            "PAGER_TITLE" => "Товары",
                            "PAGE_ELEMENT_COUNT" => "3",
                            "PARTIAL_PRODUCT_PROPERTIES" => "N",
                            "PRICE_CODE" => array("BASE"),
                            "PRICE_VAT_INCLUDE" => "Y",
                            "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
                            "PRODUCT_DISPLAY_MODE" => "N",
                            "PRODUCT_ID_VARIABLE" => "id",
                            "PRODUCT_PROPERTIES" => array(),
                            "PRODUCT_PROPS_VARIABLE" => "prop",
                            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                            "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
                            "PRODUCT_SUBSCRIPTION" => "Y",
                            "PROPERTY_CODE" => array("", ""),
                            "PROPERTY_CODE_MOBILE" => array(),
                            "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
                            "RCM_TYPE" => "personal",
                            "SECTION_CODE" => "",
                            "SECTION_CODE_PATH" => "",
                            "SECTION_ID" => '',
                            "SECTION_ID_VARIABLE" => "SECTION_ID",
                            "SECTION_URL" => "",
                            "SECTION_USER_FIELDS" => array("", ""),
                            "SEF_MODE" => "Y",
                            "SEF_RULE" => "",
                            "SET_BROWSER_TITLE" => "Y",
                            "SET_LAST_MODIFIED" => "N",
                            "SET_META_DESCRIPTION" => "Y",
                            "SET_META_KEYWORDS" => "Y",
                            "SET_STATUS_404" => "N",
                            "SET_TITLE" => "Y",
                            "SHOW_404" => "N",
                            "SHOW_ALL_WO_SECTION" => "N",
                            "SHOW_CLOSE_POPUP" => "N",
                            "SHOW_DISCOUNT_PERCENT" => "N",
                            "SHOW_FROM_SECTION" => "N",
                            "SHOW_MAX_QUANTITY" => "N",
                            "SHOW_OLD_PRICE" => "N",
                            "SHOW_PRICE_COUNT" => "1",
                            "SHOW_SLIDER" => "N",
                            "SLIDER_INTERVAL" => "3000",
                            "SLIDER_PROGRESS" => "N",
                            "TEMPLATE_THEME" => "blue",
                            "USE_ENHANCED_ECOMMERCE" => "N",
                            "USE_MAIN_ELEMENT_SECTION" => "N",
                            "USE_PRICE_COUNT" => "N",
                            "USE_PRODUCT_QUANTITY" => "N"
                        )
                    ); ?>
                </div>
            </li>
        <? endif; ?>
        <? if (!empty($arResult['PROPERTIES']['PROCHEE']['VALUE'])): ?>
            <? global $arrSectFilter4;
            ?>
            <li class="related-sections-item prochee">
                <img
                        class="related-section-image"
                        src="/upload/iblock/43e/43eb89ac7d9e6bd4aa68cd45484478ae.jpg"
                        alt="<?= GetMessage("CATALOG_PROCHEE") ?>">
                <span class="related-section-name"><?= GetMessage("CATALOG_PROCHEE") ?></span>
                <? $arrSectFilter4 = Array("ID" => $arResult['PROPERTIES']['PROCHEE']['VALUE']); ?>
                <div id="prochee" class="section_popup">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:catalog.section",
                        "table",
                        Array(
                            "ACTION_VARIABLE" => "action",
                            "ADD_PICT_PROP" => "-",
                            "ADD_PROPERTIES_TO_BASKET" => "Y",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "ADD_TO_BASKET_ACTION" => "ADD",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "BACKGROUND_IMAGE" => "-",
                            "BASKET_URL" => "/personal/basket.php",
                            "BROWSER_TITLE" => "-",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "Y",
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "COMPATIBLE_MODE" => "Y",
                            "CONVERT_CURRENCY" => "N",
                            "CUSTOM_FILTER" => "",
                            "DETAIL_URL" => "",
                            "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "DISPLAY_COMPARE" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "ELEMENT_SORT_FIELD" => "sort",
                            "ELEMENT_SORT_FIELD2" => "id",
                            "ELEMENT_SORT_ORDER" => "asc",
                            "ELEMENT_SORT_ORDER2" => "desc",
                            "ENLARGE_PRODUCT" => "STRICT",
                            "FILTER_NAME" => "arrSectFilter4",
                            "HIDE_NOT_AVAILABLE" => "N",
                            "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                            "IBLOCK_ID" => "3",
                            "IBLOCK_TYPE" => "catalog",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "LABEL_PROP" => array(),
                            "LAZY_LOAD" => "N",
                            "LINE_ELEMENT_COUNT" => "3",
                            "LOAD_ON_SCROLL" => "N",
                            "MESSAGE_404" => "",
                            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                            "MESS_BTN_BUY" => "Купить",
                            "MESS_BTN_DETAIL" => "Подробнее",
                            "MESS_BTN_SUBSCRIBE" => "Подписаться",
                            "MESS_NOT_AVAILABLE" => "Нет в наличии",
                            "META_DESCRIPTION" => "-",
                            "META_KEYWORDS" => "-",
                            "OFFERS_CART_PROPERTIES" => array(),
                            "OFFERS_FIELD_CODE" => array("", ""),
                            "OFFERS_LIMIT" => "5",
                            "OFFERS_PROPERTY_CODE" => array("", ""),
                            "OFFERS_SORT_FIELD" => "sort",
                            "OFFERS_SORT_FIELD2" => "id",
                            "OFFERS_SORT_ORDER" => "asc",
                            "OFFERS_SORT_ORDER2" => "desc",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => ".default",
                            "PAGER_TITLE" => "Товары",
                            "PAGE_ELEMENT_COUNT" => "3",
                            "PARTIAL_PRODUCT_PROPERTIES" => "N",
                            "PRICE_CODE" => array("BASE"),
                            "PRICE_VAT_INCLUDE" => "Y",
                            "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
                            "PRODUCT_DISPLAY_MODE" => "N",
                            "PRODUCT_ID_VARIABLE" => "id",
                            "PRODUCT_PROPERTIES" => array(),
                            "PRODUCT_PROPS_VARIABLE" => "prop",
                            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                            "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
                            "PRODUCT_SUBSCRIPTION" => "Y",
                            "PROPERTY_CODE" => array("", ""),
                            "PROPERTY_CODE_MOBILE" => array(),
                            "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
                            "RCM_TYPE" => "personal",
                            "SECTION_CODE" => "",
                            "SECTION_CODE_PATH" => "",
                            "SECTION_ID" => '',
                            "SECTION_ID_VARIABLE" => "SECTION_ID",
                            "SECTION_URL" => "",
                            "SECTION_USER_FIELDS" => array("", ""),
                            "SEF_MODE" => "Y",
                            "SEF_RULE" => "",
                            "SET_BROWSER_TITLE" => "Y",
                            "SET_LAST_MODIFIED" => "N",
                            "SET_META_DESCRIPTION" => "Y",
                            "SET_META_KEYWORDS" => "Y",
                            "SET_STATUS_404" => "N",
                            "SET_TITLE" => "Y",
                            "SHOW_404" => "N",
                            "SHOW_ALL_WO_SECTION" => "N",
                            "SHOW_CLOSE_POPUP" => "N",
                            "SHOW_DISCOUNT_PERCENT" => "N",
                            "SHOW_FROM_SECTION" => "N",
                            "SHOW_MAX_QUANTITY" => "N",
                            "SHOW_OLD_PRICE" => "N",
                            "SHOW_PRICE_COUNT" => "1",
                            "SHOW_SLIDER" => "N",
                            "SLIDER_INTERVAL" => "3000",
                            "SLIDER_PROGRESS" => "N",
                            "TEMPLATE_THEME" => "blue",
                            "USE_ENHANCED_ECOMMERCE" => "N",
                            "USE_MAIN_ELEMENT_SECTION" => "N",
                            "USE_PRICE_COUNT" => "N",
                            "USE_PRODUCT_QUANTITY" => "N"
                        )
                    ); ?>
                </div>
            </li>
        <? endif; ?>
        <? if (!empty($arResult['PROPERTIES']['FOR_MOUNTING']['VALUE'])): ?>
            <?
            global $arrSectFilter5;
            ?>
            <li class="related-sections-item rings">
                <img
                        class="related-section-image"
                        src=" /upload/iblock/5b1/5b1718af1a3f7d3ff804a62c0e0dffea.jpg"
                        alt="<?= GetMessage("CATALOG_MOUNT") ?>">
                <span class="related-section-name"><?= GetMessage("CATALOG_MOUNT") ?>
                </span>
                <? $arrSectFilter5 = Array("ID" => $arResult['PROPERTIES']['FOR_MOUNTING']['VALUE']); ?>
                <div id="rings" class="section_popup">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:catalog.section",
                        "table",
                        Array(
                            "ACTION_VARIABLE" => "action",
                            "ADD_PICT_PROP" => "-",
                            "ADD_PROPERTIES_TO_BASKET" => "Y",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "ADD_TO_BASKET_ACTION" => "ADD",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "BACKGROUND_IMAGE" => "-",
                            "BASKET_URL" => "/personal/basket.php",
                            "BROWSER_TITLE" => "-",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "Y",
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "COMPATIBLE_MODE" => "Y",
                            "CONVERT_CURRENCY" => "N",
                            "CUSTOM_FILTER" => "",
                            "DETAIL_URL" => "",
                            "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "DISPLAY_COMPARE" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "ELEMENT_SORT_FIELD" => "sort",
                            "ELEMENT_SORT_FIELD2" => "id",
                            "ELEMENT_SORT_ORDER" => "asc",
                            "ELEMENT_SORT_ORDER2" => "desc",
                            "ENLARGE_PRODUCT" => "STRICT",
                            "FILTER_NAME" => "arrSectFilter5",
                            "HIDE_NOT_AVAILABLE" => "N",
                            "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                            "IBLOCK_ID" => "3",
                            "IBLOCK_TYPE" => "catalog",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "LABEL_PROP" => array(),
                            "LAZY_LOAD" => "N",
                            "LINE_ELEMENT_COUNT" => "3",
                            "LOAD_ON_SCROLL" => "N",
                            "MESSAGE_404" => "",
                            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                            "MESS_BTN_BUY" => "Купить",
                            "MESS_BTN_DETAIL" => "Подробнее",
                            "MESS_BTN_SUBSCRIBE" => "Подписаться",
                            "MESS_NOT_AVAILABLE" => "Нет в наличии",
                            "META_DESCRIPTION" => "-",
                            "META_KEYWORDS" => "-",
                            "OFFERS_CART_PROPERTIES" => array(),
                            "OFFERS_FIELD_CODE" => array("", ""),
                            "OFFERS_LIMIT" => "5",
                            "OFFERS_PROPERTY_CODE" => array("", ""),
                            "OFFERS_SORT_FIELD" => "sort",
                            "OFFERS_SORT_FIELD2" => "id",
                            "OFFERS_SORT_ORDER" => "asc",
                            "OFFERS_SORT_ORDER2" => "desc",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => ".default",
                            "PAGER_TITLE" => "Товары",
                            "PAGE_ELEMENT_COUNT" => "3",
                            "PARTIAL_PRODUCT_PROPERTIES" => "N",
                            "PRICE_CODE" => array("BASE"),
                            "PRICE_VAT_INCLUDE" => "Y",
                            "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
                            "PRODUCT_DISPLAY_MODE" => "N",
                            "PRODUCT_ID_VARIABLE" => "id",
                            "PRODUCT_PROPERTIES" => array(),
                            "PRODUCT_PROPS_VARIABLE" => "prop",
                            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                            "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
                            "PRODUCT_SUBSCRIPTION" => "Y",
                            "PROPERTY_CODE" => array("", ""),
                            "PROPERTY_CODE_MOBILE" => array(),
                            "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
                            "RCM_TYPE" => "personal",
                            "SECTION_CODE" => "",
                            "SECTION_CODE_PATH" => "",
                            "SECTION_ID" => '',
                            "SECTION_ID_VARIABLE" => "SECTION_ID",
                            "SECTION_URL" => "",
                            "SECTION_USER_FIELDS" => array("", ""),
                            "SEF_MODE" => "Y",
                            "SEF_RULE" => "",
                            "SET_BROWSER_TITLE" => "Y",
                            "SET_LAST_MODIFIED" => "N",
                            "SET_META_DESCRIPTION" => "Y",
                            "SET_META_KEYWORDS" => "Y",
                            "SET_STATUS_404" => "N",
                            "SET_TITLE" => "Y",
                            "SHOW_404" => "N",
                            "SHOW_ALL_WO_SECTION" => "N",
                            "SHOW_CLOSE_POPUP" => "N",
                            "SHOW_DISCOUNT_PERCENT" => "N",
                            "SHOW_FROM_SECTION" => "N",
                            "SHOW_MAX_QUANTITY" => "N",
                            "SHOW_OLD_PRICE" => "N",
                            "SHOW_PRICE_COUNT" => "1",
                            "SHOW_SLIDER" => "N",
                            "SLIDER_INTERVAL" => "3000",
                            "SLIDER_PROGRESS" => "N",
                            "TEMPLATE_THEME" => "blue",
                            "USE_ENHANCED_ECOMMERCE" => "N",
                            "USE_MAIN_ELEMENT_SECTION" => "N",
                            "USE_PRICE_COUNT" => "N",
                            "USE_PRODUCT_QUANTITY" => "N"
                        )
                    ); ?>
                </div>
            </li>
        <? endif; ?>
    </ul>
</div>


<script>
    $(document).ready(() => {
        // Выбираем целевой элемент
        const target = document.querySelector('.detail_price');

        $('.detail_price').each((el, val) => {
            if (!val.classList.contains("hidden")) {
                const id = val.id.split("detail_price_")[1];
                $(`li.offer_picture_${id}`).removeClass("hidden");
                $(`li.offer_article_${id}`).removeClass("hidden");
                $(`li.offer_prop_${id}`).removeClass("hidden");
                $(`li.offer_title_${id}`).removeClass("hidden");
                $(`.offer_accessories_${id}`).removeClass("hidden")
            }
        });

        // Конфигурация observer (за какими изменениями наблюдать)
        const config = {
            attributes: true,
            childList: false,
            subtree: false
        };

        // Функция обратного вызова при срабатывании мутации
        const callback = function (mutationsList, observer) {
                for (let mutation of mutationsList) {
                    if (mutation.type === 'attributes') {
                        $('.detail_price').each((el, val) => {
                            if (!val.classList.contains("hidden")) {
                                const id = val.id.split("detail_price_")[1];
                                $(`li[id^=offer_picture_${id}]`).removeClass("hidden");
                                $(`li[class*=offer_article_${id}]`).each((el, val) => $(val).removeClass("hidden"));
                                $(`li[class*=offer_prop_${id}]`).each((el, val) => $(val).removeClass("hidden"));
                                $(`li[id^=offer_title_${id}]`).removeClass("hidden");
                                $(`div[class^=offer_accessories_${id}]`).removeClass("hidden");
                                checkPrice();
                            } else {
                                const id = val.id.split("detail_price_")[1];
                                $(`li[id^=offer_picture_${id}]`).addClass("hidden");
                                $(`li[class*=offer_article_${id}]`).each((el, val) => $(val).addClass("hidden"));
                                $(`li[class*=offer_prop_${id}]`).each((el, val) => $(val).addClass("hidden"));
                                $(`li[id^=offer_title_${id}]`).addClass("hidden");
                                $(`div[class^=offer_accessories_${id}]`).addClass("hidden");
                            }
                        });
                    }
                }
            }
        ;

        // Создаем экземпляр наблюдателя с указанной функцией обратного вызова
        const observer = new MutationObserver(callback);

        // Начинаем наблюдение за настроенными изменениями целевого элемента
        observer.observe(target, config);

    })
</script>
<script type="text/javascript">
    //<![CDATA[

    function checkPrice() {
        const offerPrice = $('.detail_price:not(".hidden") .catalog-detail-item-price').attr("data-price");
        let setPrice = Number(offerPrice);
        $('.accessories_set').each((n, el) => {
            if (!el.classList.contains("hidden")) {

                $(el).find('.catalog-item-info').each((n, el) => {
                    if ($(el).find(".quantity")[0].value > 0) {
                        setPrice += Number($(el).find(".quantity")[0].value) * parseInt($(el).find(".catalog-item-price")[0].innerHTML);
                    }
                });
            }
        });

        if ($('#product-set-price')[0]) {
            $('#product-set-price')[0].innerHTML = setPrice;
        }
    }

    $(document).ready(() => {
            <?if(isset($arResult["OFFERS"]) && !empty($arResult["OFFERS"])):
            if($arSetting["OFFERS_VIEW"]["VALUE"] == "LIST"):
            foreach($arResult["OFFERS"] as $key => $arOffer):?>
            $("#catalog-offer-item-<?=$arOffer['ID']?> .catalog-item-prop").clone().appendTo("#catalog-offer-item-<?=$arOffer['ID']?> .catalog-item-props-mob");
            <?endforeach;
            endif;
            endif;?>
            $("#set-constructor-items-from").appendTo("#set-constructor-items-to").css({"display": "block"});
            $("#accessories-from").appendTo("#accessories-to").css({"display": "block"});
            $("#catalog-reviews-from").appendTo("#catalog-reviews-to").css({"display": "block"});
            $(".add2basket_form").submit(function () {
                if ($('#set_price')[0]) {
                    if ($('#set_price')[0].checked) {
                        $('.accessories_set').each((n, el) => {
                            console.log(el)
                            if (!el.classList.contains("hidden")) {
                                $(el).find('.acessorie_item_form').each((n, form) => {
                                    if ($(form).find(".quantity").attr("value") > 0) {
                                        $.post($(form).attr("action"), $(form).serialize());
                                    }
                                })
                            }
                        });
                    }
                }


                const form = $(this);
                imageItem = form.find(".item_image").attr("value");
                $("#addItemInCart .item_image_full").html(imageItem);

                titleItem = form.find(".item_title").attr("value");
                $("#addItemInCart .item_title").text(titleItem);

                var ModalName = $("#addItemInCart");
                CentriredModalWindow(ModalName);
                OpenModalWindow(ModalName);

                $.post($(this).attr("action"), $(this).serialize(), function (data) {
                    try {
                        $.post("/ajax/basket_line.php", function (data) {
                            refreshCartLine(data);
                        });
                        $.post("/ajax/delay_line.php", function (data) {
                            $(".delay_line").replaceWith(data);
                        });
                        form.children(".btn_buy").addClass("hidden");
                        form.children(".result").removeClass("hidden");
                    } catch (e) {
                    }
                });
                return false;
            });


            //Accessories  pop-up
            if ($("#product-set_pop-up")[0].childElementCount!=0) {
                const showProductSet = new BX.PopupWindow("set_pop-up", BX('product-set'), {
                    content: BX('product-set_pop-up'), //контент pop-up
                    closeIcon: {right: "20px", top: "10px"},
                    angle: true,
                    zIndex: 0,
                });
                $('.product-set_detail').click(function () {
                    showProductSet.show(); // появление окна
                });
            } else {
                $("#product-set").css("display", "none")
            }

            //Accessories
            checkPrice();

            $(".accessories-minus").click((e) => {
                let doorPrice;
                let quantity = $(e.target).closest(".qnt_cont").find('.quantity')[0].value;

                $(".detail_price").each((n, el) => {
                    if (!el.classList.contains("hidden")) {
                        doorPrice = el;
                    }
                });

                if (quantity > 0) {
                    $('#product-set-price')[0].innerHTML = Number($('#product-set-price')[0].innerHTML) - parseInt($(e.target).closest(".catalog-item-info").find(".catalog-item-price")[0].innerHTML);
                    $(e.target).closest(".qnt_cont").find('.quantity')[0].value = Number($(e.target).closest(".qnt_cont").find('.quantity')[0].value) - 1;
                }
            });

            $(".accessories-plus").click((e) => {
                $('#product-set-price')[0].innerHTML = Number($('#product-set-price')[0].innerHTML) + parseInt($(e.target).closest(".catalog-item-info").find(".catalog-item-price")[0].innerHTML);
                $(e.target).closest(".qnt_cont").find('.quantity')[0].value = Number($(e.target).closest(".qnt_cont").find('.quantity')[0].value) + 1;
            });
        }
    );

    //Pop-up for furniture
    $(document).ready(() => {
        $('.related-sections-item').click((e) => {
            event.preventDefault();
            let content;
            if (e.target.closest(".related-sections-item").classList.contains("rings")) {
                content = BX('rings');
            } else if (e.target.closest(".related-sections-item").classList.contains("locks")) {
                content = BX('locks');
            } else if (e.target.closest(".related-sections-item").classList.contains("tsylindry")) {
                content = BX('tsylindry');
            } else if (e.target.closest(".related-sections-item").classList.contains("nakladki")) {
                content = BX('nakladki');
            } else if (e.target.closest(".related-sections-item").classList.contains("prochee")) {
                content = BX('prochee');
            }

            const popup = new BX.PopupWindow("", null, {
                content: content,
                closeIcon: {right: "20px", top: "10px"},
                zIndex: 0,
                offsetLeft: 0,
                offsetTop: 0,
                draggable: {restrict: false},
            });
            popup.show(); // появление окна
        });
    });


    //]]>
</script>
