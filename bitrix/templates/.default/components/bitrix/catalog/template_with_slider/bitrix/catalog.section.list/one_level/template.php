<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true); ?>

<!-- Проверка - являются ли секции - секциями первого уровня -->
<?if ($arResult["IS_PARENT_SECTIONS"]):?>
<div class="catalog-section-list">
    <? foreach ($arResult["PARENT_SECTIONS"] as $arSection): ?>
        <div class="catalog-section-childs">
            <div class="catalog-section-child">
                <a href="<?= $arSection["SECTION_PAGE_URL"] ?>">
				    <span class="child">
					    <span class="text"><?= $arSection['NAME'] ?></span>
                    </span>
                </a>
            </div>
            <div class="clr"></div>
        </div>
    <? endforeach; ?>
</div>
<?else: ?>
<div class="catalog-section-list">
    <? foreach ($arResult["SECTIONS"] as $arSection): ?>
        <div class="catalog-section-childs">
            <? foreach ($arSection['CHILDREN'] as $key => $arChild): ?>
                <div class="catalog-section-child">
                    <a href="<?= $arChild["SECTION_PAGE_URL"] ?>">
								<span class="child">
									<span class="text"><?= $arChild['NAME'] ?></span>
								</span>
                    </a>
                </div>
            <? endforeach; ?>
            <div class="clr"></div>
        </div>
    <? endforeach; ?>
</div>
<?endif; ?>

<script type="text/javascript">
    //<![CDATA[
    $(function () {
        $(".showchild").click(function () {
            var clickitem = $(this);
            if (clickitem.parent("div").hasClass('active')) {
                clickitem.parent("div").removeClass("active");
            } else {
                clickitem.parent("div").addClass("active");
            }
            clickitem.parent("div").parent("div").find(".catalog-section-childs").slideToggle();
        });
    });
    //]]>
</script>