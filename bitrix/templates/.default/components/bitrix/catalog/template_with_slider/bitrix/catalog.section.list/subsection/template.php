<link rel="stylesheet" href="/bitrix/css/main/bootstrap_v4/bootstrap.min.css">

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

if(count($arResult["SECTIONS"]) < 1)
    return;
?>

<div class="catalog-section-list">
    <?foreach($arResult["SECTIONS"] as $arSection):

        $bHasChildren = is_array($arSection['CHILDREN']) && count($arSection['CHILDREN']) > 0;?>

        <div class="catalog-section">
            <?if($arSection['NAME'] && $arResult['SECTION']['ID'] != $arSection['ID']):?>
                <div class="catalog-section-title" <?if($bHasChildren):?>style="margin:0px 0px 4px 0px;"<?else:?>style="margin:0px 0px 2px 0px;"<?endif;?>>
                    <a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?></a>
                </div>
            <?endif;?>
            <?if($bHasChildren):?>
                <div class="catalog-section-childs container-fluid testimonial-group">
                    <div class="row flex-nowrap">
                        <div class="col-auto col-md-12">
                            <div class="container-fluid">
                                <div class="row">
                                    <?
                                    $i = 0;
                                    $sectionNumber = count($arSection['CHILDREN']); ?>
                                    <?foreach($arSection['CHILDREN'] as $key => $arChild) {?>
                                        <div class="catalog-section-child">
                                            <a href="<?=$arChild["SECTION_PAGE_URL"]?>">
                                            <span class="child">
                                                <span class="text"><?=$arChild['NAME']?></span>
                                            </span>
                                            </a>
                                        </div>
                                        <?if($i===intval(($sectionNumber)/2)) {?>

                                            <?if ($sectionNumber > 1) {?>
                                                <div class="w-100 d-md-none d-block"></div>
                                                <?
                                            }
                                        }
                                        $i++;
                                    }?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clr"></div>
                </div>
            <?endif;?>
        </div>

    <?endforeach;?>
</div>