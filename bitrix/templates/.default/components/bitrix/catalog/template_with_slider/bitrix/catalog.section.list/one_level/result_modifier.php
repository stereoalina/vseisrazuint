<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

// Проверка - являются ли секции - секциями первого уровня
$arResult["IS_PARENT_SECTIONS"] = false;
// Получить секции первого уровня
$arFilter = Array('IBLOCK_ID'=>$arParams["IBLOCK_ID"], 'GLOBAL_ACTIVE'=>'Y', 'DEPTH_LEVEL' => 1);
$db_list = CIBlockSection::GetList(Array(), $arFilter, true);
while($ar_result = $db_list->GetNext())
{
    // Если текущая страница - секция первого уровня
    if (strpos($APPLICATION->GetCurDir(), $ar_result['SECTION_PAGE_URL'])!== false) {
        // Будут выведены только секции первого уровня
        $arResult["IS_PARENT_SECTIONS"] = true;
    }
    // Здесь хранятся секции первого уровня
    $arResult["PARENT_SECTIONS"][] = $ar_result;
}

$arSections = array();

$arFilter = Array('IBLOCK_ID'=>$arParams["IBLOCK_ID"], 'GLOBAL_ACTIVE'=>'Y','SECTION_ID' => $arResult['SECTION']['IBLOCK_SECTION_ID']);
$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);

while($ar_result = $db_list->GetNext())
{
    $arResult["SECTIONS"][] = $ar_result;
}

foreach($arResult["SECTIONS"] as $key => $arSection):
	if($arSection["IBLOCK_SECTION_ID"] > 0):
		$arSections[$arSection["IBLOCK_SECTION_ID"]]["CHILDREN"][$arSection["ID"]] = $arSection;
	else:
		$arSection["CHILDREN"] = array();
		$arSections[$arSection["ID"]] = $arSection;
	endif;
endforeach;

$arResult["SECTIONS"] = $arSections;

foreach($arResult["SECTIONS"] as $key => $arSection):
	if(isset($arSection["CHILDREN"]) && count($arSection["CHILDREN"]) > 0):
		foreach($arSection["CHILDREN"] as $keyChild => $arChild):
			if(is_array($arChild["PICTURE"])):
				$arFileTmp = CFile::ResizeImageGet(
					$arChild["PICTURE"],
					array("width" => $arParams["DISPLAY_IMG_WIDTH"], "height" => $arParams["DISPLAY_IMG_HEIGHT"]),
					BX_RESIZE_IMAGE_PROPORTIONAL,
					true
				);

				$arResult["SECTIONS"][$key]["CHILDREN"][$keyChild]["PICTURE_PREVIEW"] = array(
					"SRC" => $arFileTmp["src"],
					"WIDTH" => $arFileTmp["width"],
					"HEIGHT" => $arFileTmp["height"],
				);
			endif;
		endforeach;
	endif;
endforeach;?>